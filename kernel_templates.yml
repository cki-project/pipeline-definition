---

# Default kernel repo workflow
.workflow:
  rules:
    - if: '$CI_MERGE_REQUEST_ID && $CI_PROJECT_ID != $CI_MERGE_REQUEST_PROJECT_ID'
      variables:
        REQUESTED_PIPELINE_TYPE: 'insufficient-permissions'
    - if: '$CI_MERGE_REQUEST_PROJECT_PATH =~ /^redhat.rhel/ || $CI_PROJECT_PATH =~ /^redhat.rhel/'
      variables:
        REQUESTED_PIPELINE_TYPE: 'internal'
    - if: '$CI_MERGE_REQUEST_PROJECT_PATH =~ /^redhat.centos-stream/ || $CI_PROJECT_PATH =~ /^redhat.centos-stream/ ||
           $CI_MERGE_REQUEST_PROJECT_PATH =~ /^CentOS/ || $CI_PROJECT_PATH =~ /^CentOS/'
      variables:
        REQUESTED_PIPELINE_TYPE: '/^(trusted|centos-rhel)$/'
    - if: '$CI_MERGE_REQUEST_PROJECT_PATH =~ /^redhat.prdsc/ || $CI_PROJECT_PATH =~ /^redhat.prdsc/'
      variables:
        REQUESTED_PIPELINE_TYPE: 'scratch'

# explicitly fail pipelines running in a fork instead of the parent repo
# most likely a click on https://red.ht/GitLabSSO is needed for RH employees
insufficient-permissions:
  trigger:
    project: redhat/red-hat-ci-tools/kernel/insufficient-permissions
    strategy: depend
  rules:
    - if: '$REQUESTED_PIPELINE_TYPE == "insufficient-permissions"'

# Merge request pipeline
.merge_request:
  variables:
    title: ${CI_COMMIT_TITLE}
    commit_hash: ${CI_COMMIT_SHA}
    git_url: ${CI_MERGE_REQUEST_PROJECT_URL}.git
    branch: ${CI_MERGE_REQUEST_TARGET_BRANCH_NAME}
    mr_id: ${CI_MERGE_REQUEST_IID}
    mr_url: ${CI_MERGE_REQUEST_PROJECT_URL}/-/merge_requests/${CI_MERGE_REQUEST_IID}
    mr_title: ${CI_MERGE_REQUEST_TITLE}
    mr_project_path: ${CI_MERGE_REQUEST_PROJECT_PATH}
    mr_project_id: ${CI_MERGE_REQUEST_PROJECT_ID}
    mr_source_branch_hash: ${CI_MERGE_REQUEST_SOURCE_BRANCH_SHA}  # only set on merged result pipelines
    mr_pipeline_id: ${CI_PIPELINE_ID}
    trigger_job_name: ${CI_JOB_NAME}
  rules:
    - if: '$PIPELINE_TYPE =~ $REQUESTED_PIPELINE_TYPE && $CI_MERGE_REQUEST_ID && (
             ($RUN_ONLY_FOR_RT =~ /^[Tt]rue$/ && $CI_MERGE_REQUEST_TARGET_BRANCH_NAME =~ /-rt$/) ||
             ($RUN_ONLY_FOR_AUTOMOTIVE =~ /^[Tt]rue$/ && $CI_MERGE_REQUEST_TARGET_BRANCH_NAME =~ /-automotive$/) ||
             (
               ($RUN_ONLY_FOR_RT !~ /^[Tt]rue$/ && $CI_MERGE_REQUEST_TARGET_BRANCH_NAME !~ /-rt$/) &&
               ($RUN_ONLY_FOR_AUTOMOTIVE !~ /^[Tt]rue$/ && $CI_MERGE_REQUEST_TARGET_BRANCH_NAME !~ /-automotive$/)
             )
          )'

# Baseline pipeline
.baseline:
  variables:
    title: ${CI_COMMIT_TITLE}
    commit_hash: ${CI_COMMIT_SHA}
    git_url: ${CI_PROJECT_URL}.git
    branch: ${CI_COMMIT_BRANCH}
    test_set: 'boot'
    scratch: 'false'
    trigger_job_name: ${CI_JOB_NAME}
  rules:
    - if: '$PIPELINE_TYPE =~ $REQUESTED_PIPELINE_TYPE && $CI_COMMIT_BRANCH && (
             ($RUN_ONLY_FOR_RT =~ /^[Tt]rue$/ && $CI_COMMIT_BRANCH =~ /-rt$/) ||
             ($RUN_ONLY_FOR_AUTOMOTIVE =~ /^[Tt]rue$/ && $CI_COMMIT_BRANCH =~ /-automotive$/) ||
             (
               ($RUN_ONLY_FOR_RT !~ /^[Tt]rue$/ && $CI_COMMIT_BRANCH !~ /-rt$/) &&
               ($RUN_ONLY_FOR_AUTOMOTIVE !~ /^[Tt]rue$/ && $CI_COMMIT_BRANCH !~ /-automotive$/)
             )
          )'

# Trigger internal child pipeline
.internal:
  trigger:
    project: redhat/red-hat-ci-tools/kernel/cki-internal-pipelines/cki-internal-contributors
    strategy: depend
  variables:
    PIPELINE_TYPE: 'internal'
    # use fixed git cache owner as group of kernel repositories might differ from 'kernel'
    git_url_cache_owner: kernel

.centos_stream_rhel_internal:
  extends: .internal
  variables:
    PIPELINE_TYPE: 'centos-rhel'

# Trigger trusted child pipeline
.trusted:
  trigger:
    project: redhat/red-hat-ci-tools/kernel/cki-internal-pipelines/cki-trusted-contributors
    strategy: depend
  variables:
    PIPELINE_TYPE: 'trusted'

# Trigger scratch child pipeline
.scratch:
  trigger:
    project: redhat/red-hat-ci-tools/kernel/cki-scratch-pipelines/scratch-pipelines
    strategy: depend
  variables:
    PIPELINE_TYPE: 'scratch'
    git_url_cache_owner: kernel
    test_priority: 'urgent'

# Only build and publish, exclude setup, test and results
.only_build_and_publish:
  variables:
    skip_setup: 'true'  # No need to send pre-test messages
    skip_test: 'true'  # Skip testing, only need to look out for merge/build problems
    skip_results: 'true'  # Any issues should be caused by conflicts

# Obsolete, but in use and cannot be empty
.with_notifications:
  variables: {}

# Coverage variables
.coverage:
  variables:
    coverage: 'true'
    # coverage for debug builds is not supported by the spec file
    debug_architectures: ''

# RHEL10 variants
.variant_up:
  variables:
    rpmbuild_with: up zfcpdump base
    package_name: kernel
    architectures: x86_64 aarch64 s390x ppc64le
    run_redhat_self_test: 'true'
.variant_up_debug:
  variables:
    rpmbuild_with: up debug
    package_name: kernel-debug
    architectures: x86_64 aarch64 s390x ppc64le
.variant_rt:
  variables:
    rpmbuild_with: realtime base
    package_name: kernel-rt
    architectures: x86_64 aarch64
.variant_rt_debug:
  variables:
    rpmbuild_with: realtime debug
    package_name: kernel-rt-debug
    architectures: x86_64 aarch64
.variant_rt_64k:
  variables:
    rpmbuild_with: realtime_arm64_64k base
    package_name: kernel-rt-64k
    architectures: aarch64
    kpet_extra_components: '64kpages'
.variant_rt_64k_debug:
  variables:
    rpmbuild_with: realtime_arm64_64k debug
    package_name: kernel-rt-64k-debug
    architectures: aarch64
    kpet_extra_components: '64kpages'
.variant_automotive:
  variables:
    rpmbuild_with: automotive base
    package_name: kernel-automotive
    architectures: x86_64 aarch64
  allow_failure: true
.variant_automotive_debug:
  variables:
    rpmbuild_with: automotive debug
    package_name: kernel-automotive-debug
    architectures: x86_64 aarch64
  allow_failure: true
.variant_64k:
  variables:
    rpmbuild_with: arm64_64k base
    package_name: kernel-64k
    architectures: aarch64
    kpet_extra_components: '64kpages'
.variant_64k_debug:
  variables:
    rpmbuild_with: arm64_64k debug
    package_name: kernel-64k-debug
    architectures: aarch64
    kpet_extra_components: '64kpages'

# Templates for RHEL <= 9

# Common RHEL settings
.rhel_common:
  variables:
    debug_architectures: 'x86_64'
    rpmbuild_with: 'up zfcpdump'
    source_package_name: kernel

# Common realtime_check settings
.realtime_check_common:
  extends: [.rhel_common]
  variables:
    package_name: kernel-rt
    source_package_name: kernel-rt
    rpmbuild_with: 'up realtime'
    merge_branch: ${CI_MERGE_REQUEST_TARGET_BRANCH_NAME}-rt
    architectures: 'x86_64'  # Supported on x86_64 only
    build_noarch: 'false'  # no noarch packages

# Common full RT pipeline settings (<9.3)
.realtime_pipeline_common:
  extends: .rhel_common
  variables:
    package_name: kernel-rt
    source_package_name: kernel-rt
    rpmbuild_with: 'up realtime'
    architectures: 'x86_64'  # Supported on x86_64 only
    build_noarch: 'false'  # no noarch packages

# Common kernel-rt settings (>=9.3)
.rt_common:
  extends: .rhel_common
  variables:
    package_name: kernel-rt
    source_package_name: kernel
    rpmbuild_with: 'realtime'
    architectures: x86_64

# RHEL >= 9.6 enables aarch64 for RT as well
.rt_gte_96_common:
  extends: .rt_common
  variables:
    architectures: x86_64 aarch64

# Common kernel-rt-64k settings
.rt_64k_common:
  extends: .rhel_common
  variables:
    package_name: kernel-rt-64k
    source_package_name: kernel
    rpmbuild_with: 'realtime_arm64_64k'
    architectures: 'aarch64'
    kpet_extra_components: '64kpages'

# Common kernel-automotive settings (>= c10s)
.automotive_common:
  extends: .rhel_common
  variables:
    package_name: kernel-automotive
    source_package_name: kernel
    rpmbuild_with: 'automotive'
    architectures: 'x86_64 aarch64'  # Supported on x86_64/arm only

# Common kernel-64k settings
.64k_common:
  variables:
    package_name: kernel-64k
    source_package_name: kernel
    architectures: 'aarch64'
    rpmbuild_with: 'arm64_64k'
    kpet_extra_components: '64kpages'

# Common kernel-debug settings
.debug_common:
  variables:
    package_name: kernel-debug
    source_package_name: kernel
    rpmbuild_with: 'debug'
    kpet_extra_components: 'debugbuild'

# Common full automotive pipeline settings (AutoSD 9)
.automotive_pipeline_common:
  extends: .rhel_common
  variables:
    package_name: kernel-automotive
    source_package_name: kernel-automotive
    architectures: 'x86_64 aarch64'  # Supported on x86_64/arm only
    debug_architectures: 'x86_64 aarch64'
    test_runner: aws
    test_set: automotive
    build_noarch: 'false'  # no noarch packages

# Automotive check configs
.automotive_check_common:
  extends: .automotive_pipeline_common
  allow_failure: true  # Don't block MRs!
  variables:
    merge_branch: ${CI_MERGE_REQUEST_TARGET_BRANCH_NAME}-automotive
    pipeline_name_suffix: '(results are not blocking the MR)'
