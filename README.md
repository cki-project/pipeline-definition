# pipeline-definition

YAMLs for CKI pipeline definition

# Pipeline linting and unit tests

There is a `cki_tools.gitlab_yaml_shellcheck` linter script in [cki-tools] that
allows to lint the shell code embedded in the pipeline. This script is also
used in the GitLab CI/CD pipeline.

To run the linting locally use:

```bash
# Lint .sh files from tests
podman run --pull=newer --rm -it --volume .:/data:Z --workdir /data \
    quay.io/cki/cki-tools:production \
    find ./tests ./spec -name '*.sh' -exec shellcheck --external-sources --enable=all {} +
# Lint scripts within cki_pipeline.yml
podman run --pull=newer --rm -it --volume .:/data:Z --workdir /data \
    quay.io/cki/cki-tools:production \
    ./lint.sh
```

In GitLab CI/CD, the tests run on all container images used in CKI pipelines,
which consists of `quay.io/cki/python` and all builder container images.
This can be simulated locally with the calls below,
just replace `quay.io/cki/builder-rawhide` with the appropriate image.

```bash
# Run ad-hoc tests
podman run --pull=newer --rm -it --volume .:/data:Z --workdir /data \
    quay.io/cki/builder-rawhide:production \
    ./tests.sh
# Run shellspec tests
podman run --pull=newer --rm -it --volume .:/data:Z --workdir /data \
    quay.io/cki/builder-rawhide:production \
    shellspec
```

Note: You can execute a specific ad-hoc tests from the `./tests` folder.
To run a subset of shellspec tests, e.g. within `Describe 'is_debug_build'`:

```bash
podman run --pull=newer --rm -it --volume .:/data:Z --workdir /data \
    quay.io/cki/builder-rawhide:production \
    shellspec spec/functions_general_spec.sh --example "is_debug_build*"
```

# Pipeline testing

The preferred way is to make an MR and ask `cki-ci-bot` to test the changes. It
is also possible to use `retrigger` from [cki-tools]. This is what `cki-bot`
uses on the background too. You need to have valid tokens to use this method.

See the documentation on [pipeline triggering] for more details.

[cki-tools]: https://gitlab.com/cki-project/cki-tools
[pipeline triggering]: https://cki-project.org/l/pipeline-triggering/
