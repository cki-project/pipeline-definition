#!/bin/bash
# shellcheck disable=SC2312         # masking of return values
# shellcheck disable=SC2317         # unreachable mocking code

eval "$(shellspec - -c) exit 1"

Describe 'default setting in test plan'
    Include tests/source_functions.sh
    Include tests/helpers.sh
    Set 'errexit:on' 'nounset:on' 'pipefail:on'

    KCIDB_DUMPFILE_NAME=kcidb_all.json
    export DEBUG_KERNEL
    DEBUG_KERNEL="true"

    setup() {
        CI_PROJECT_DIR=$(mktemp -d)
    }
    BeforeEach 'setup'

    cleanup() {
        rm -rf "${CI_PROJECT_DIR}"
    }
    AfterEach 'cleanup'

    Context 'if there are tests in the KCIDB file'
        It "correctly sets the values"
            cp tests/assets/stub_kcidb.json "${CI_PROJECT_DIR}/${KCIDB_DUMPFILE_NAME}"

            When call kcidb_set_test_defaults
            The contents of file "${CI_PROJECT_DIR}/${KCIDB_DUMPFILE_NAME}" should include '"debug": true'
            The contents of file "${CI_PROJECT_DIR}/${KCIDB_DUMPFILE_NAME}" should include '"targeted": false'
        End
    End

    Context 'if there are no tests but empty tests list entry is present'
        It "does nothing"
            # Remove dummy test entries from the KCIDB file
            jq 'del(.tests[])' tests/assets/stub_kcidb.json > "${CI_PROJECT_DIR}/${KCIDB_DUMPFILE_NAME}"

            When call kcidb_set_test_defaults
            The contents of file "${CI_PROJECT_DIR}/${KCIDB_DUMPFILE_NAME}" should include '"tests": []'
        End
    End

    Context 'if there is no test entry'
        It "does nothing"
            # Remove dummy test entries from the KCIDB file
            jq 'del(.tests)' tests/assets/stub_kcidb.json > "${CI_PROJECT_DIR}/${KCIDB_DUMPFILE_NAME}"

            When call kcidb_set_test_defaults
            The status should be success
            The contents of file "${CI_PROJECT_DIR}/${KCIDB_DUMPFILE_NAME}" should not include '"tests"'
        End
    End
End

Describe 'kcidb checkout creation'
    Include tests/source_functions.sh
    Include tests/helpers.sh

    Set 'errexit:on' 'nounset:on' 'pipefail:on'

    export CKI_DEPLOYMENT_ENVIRONMENT
    export CI_PIPELINE_ID=987654

    Parameters
        production 'brew_task_id=123 web_url=https://dummy.url/koji' "redhat:koji-123" ""
        production 'brew_task_id=123 web_url=https://dummy.url/brew' "redhat:brew-123" ""
        production 'brew_task_id=123 brew_nvr=123 web_url=https://dummy.url/brew' "redhat:987654" ""
        retrigger 'brew_task_id=123 web_url=https://dummy.url/koji' "redhat:987654" ""
        retrigger 'brew_task_id=123 web_url=https://dummy.url/brew' "redhat:987654" ""
        retrigger 'brew_task_id=123 web_url=https://dummy.url/koji' "redhat:koji-123" "original/pipelines/987"
        retrigger 'git_url=something' "redhat:987" "original/pipeline/987"
        production 'git_url=something' "redhat:987654" ""

    End

    It "correctly creates checkout ID for ${1} ${2} (${4})"
        CKI_DEPLOYMENT_ENVIRONMENT="${1}"
        for variable in ${2} ; do
            export "${variable?}"
        done

        When call create_checkout_id "${4}"
        The output should equal "${3}"
    End
End

Describe 'miss all tests'
    Include tests/source_functions.sh
    Include tests/helpers.sh

    KCIDB_DUMPFILE_NAME=kcidb_all.json

    setup() {
        CI_PROJECT_DIR=$(mktemp -d)
        kcidb_calls_file="${CI_PROJECT_DIR}/kcidb_calls"
        touch "${kcidb_calls_file}"
    }
    BeforeEach 'setup'

    cleanup() {
        rm -rf "${CI_PROJECT_DIR}"
    }
    AfterEach 'cleanup'

    kcidb() {
        # Only save test ID, key, value
        echo "${2} ${4} ${5}" >> "${kcidb_calls_file}"
    }

    Context 'if there are tests in the KCIDB file'
        It "correctly sets the MISS status"
            # remove the status attribute from the tests in the stub file for this test
            jq 'del(.tests[].status)' ./tests/assets/stub_kcidb.json > "${CI_PROJECT_DIR}/${KCIDB_DUMPFILE_NAME}"

            When call kcidb_miss_all_tests
            The status should be success
            The contents of file "${kcidb_calls_file}" should include "redhat:1 status MISS"
            The contents of file "${kcidb_calls_file}" should include "redhat:2 status MISS"
            The contents of file "${kcidb_calls_file}" should include "redhat:3 status MISS"
        End
    End

    Context 'if there are no tests in the KCIDB file'
        It "does nothing"
            # Remove dummy test entries from the KCIDB file
            jq 'del(.tests)' tests/assets/stub_kcidb.json > "${CI_PROJECT_DIR}/${KCIDB_DUMPFILE_NAME}"

            When call kcidb_miss_all_tests
            The status should be success
            The contents of file "${CI_PROJECT_DIR}/${KCIDB_DUMPFILE_NAME}" should not include '"tests"'
        End
    End
End

Describe 'missing tests'
    Include tests/source_functions.sh
    Include tests/helpers.sh

    KCIDB_DUMPFILE_NAME=kcidb_all.json
    export DEBUG_KERNEL
    DEBUG_KERNEL="true"

    setup() {
        CI_PROJECT_DIR=$(mktemp -d)
        kcidb_calls_file="${CI_PROJECT_DIR}/kcidb_calls"
        touch "${kcidb_calls_file}"
    }
    BeforeEach 'setup'

    cleanup() {
        rm -rf "${CI_PROJECT_DIR}"
    }
    AfterEach 'cleanup'

    kcidb() {
        # Only save test ID, key, value
        echo "${2} ${4} ${5}" >> "${kcidb_calls_file}"
    }

    Context 'if there are tests in the KCIDB file'
        It "correctly sets the values"
            cp tests/assets/stub_kcidb.json "${CI_PROJECT_DIR}/${KCIDB_DUMPFILE_NAME}"

            When call kcidb_missing_tests
            The status should be success
            The output should not include "redhat:1: Status is null, setting to MISS"
            The output should include "redhat:2: Status is null, setting to MISS"
            The output should include "redhat:3: Status is null, setting to MISS"
            The contents of file "${kcidb_calls_file}" should not include "redhat:1 status MISS"
            The contents of file "${kcidb_calls_file}" should include "redhat:2 status MISS"
            The contents of file "${kcidb_calls_file}" should include "redhat:3 status MISS"
        End
    End

    Context 'if there are no tests in the KCIDB file'
        It "does nothing"
            # Remove dummy test entries from the KCIDB file
            jq 'del(.tests)' tests/assets/stub_kcidb.json > "${CI_PROJECT_DIR}/${KCIDB_DUMPFILE_NAME}"

            When call kcidb_missing_tests
            The status should be success
            The contents of file "${CI_PROJECT_DIR}/${KCIDB_DUMPFILE_NAME}" should not include '"tests"'
        End
    End
End

# Uses 'When run' to keep set -e
# https://github.com/shellspec/shellspec/blob/master/docs/references.md#comparison
Describe 'kcidb_set_modified_files_affected_subsystems'
    Include tests/source_functions.sh
    Include tests/helpers.sh
    Set 'errexit:on' 'nounset:on' 'pipefail:on'

    KCIDB_DUMPFILE_NAME=kcidb_all.json
    export VENV_PY3=stub_python
    # hack to get predictable path
    export SOFTWARE_DIR=

    setup() {
        CI_PROJECT_DIR=$(mktemp -d)
        cp tests/assets/stub_kcidb.json "${CI_PROJECT_DIR}/${KCIDB_DUMPFILE_NAME}"
        python_calls_file="${CI_PROJECT_DIR}/python_calls"
        touch "${python_calls_file}"
        mkdir "${CI_PROJECT_DIR}/artifacts"
    }
    BeforeEach 'setup'

    cleanup() {
        rm -rf "${CI_PROJECT_DIR}"
    }
    AfterEach 'cleanup'

    stub_python() {
        echo "|$*|" >> "${python_calls_file}"
        if [[ $2 == cki_lib.owners ]] && [[ "$*" = *json* ]]; then
            echo "${subsystems_output}"
        elif [[ $2 == cki_lib.owners ]] && [[ "$*" = *text* ]]; then
            if (( ${#owners_output[@]} > 0 )); then
              printf '%s\n' "${owners_output[@]}"
            fi
        elif [[ $2 == kpet ]]; then
            if [[ -n "${kpet_output}" ]]; then
                echo "${kpet_output}"
            fi
        fi
    }

    Context 'if there are no patches'
        export patch_urls=

        It "does not change the checkout without a merge request"
            When run kcidb_set_modified_files_affected_subsystems
            The status should be success
            The contents of file "${CI_PROJECT_DIR}/${KCIDB_DUMPFILE_NAME}" should not include '"patchset_modified_files"'
            The contents of file "${CI_PROJECT_DIR}/${KCIDB_DUMPFILE_NAME}" should not include '"affected_subsystems"'
            The contents of file "${python_calls_file}" should not include "|-m kpet"
            The contents of file "${python_calls_file}" should not include "|-m cki_lib.owners"
        End
    End

    Context 'if there are patches outside a merge request'
        export patch_urls="foo baz"
        kpet_output='bar'
        subsystems_output='[{"name":"MM","label":"mm"}]'
        owners_output=(@mm)

        It "adds info to the checkout"
            When run kcidb_set_modified_files_affected_subsystems
            The status should be success
            The contents of file "${CI_PROJECT_DIR}/${KCIDB_DUMPFILE_NAME}" should include '"bar"'
            The contents of file "${CI_PROJECT_DIR}/${KCIDB_DUMPFILE_NAME}" should include '"mm"'
            The contents of file "${python_calls_file}" should include "|-m kpet --db /kpet-db patch list-files foo baz|"
            The contents of file "${python_calls_file}" should include "|-m cki_lib.owners --owners-yaml-path /documentation/info/owners.yaml --output text --file-list /tmp"
            The contents of file "${python_calls_file}" should include "|-m cki_lib.owners --owners-yaml-path /documentation/info/owners.yaml --output json --file-list /tmp"
        End
    End

    Context 'if there is a merge request'
        export mr_id=123
        kpet_output='bar'
        subsystems_output='[{"name":"MM","label":"mm"}]'
        owners_output=(@mm)

        It "does not change the checkout with force_baseline"
            export force_baseline=true
            When run kcidb_set_modified_files_affected_subsystems
            The status should be success
            The contents of file "${CI_PROJECT_DIR}/${KCIDB_DUMPFILE_NAME}" should not include '"patchset_modified_files"'
            The contents of file "${CI_PROJECT_DIR}/${KCIDB_DUMPFILE_NAME}" should not include '"affected_subsystems"'
            The contents of file "${python_calls_file}" should not include "|-m kpet"
            The contents of file "${python_calls_file}" should not include "|-m cki_lib.owners"
        End

        It "adds info to the checkout"
            When run kcidb_set_modified_files_affected_subsystems
            The status should be success
            The contents of file "${CI_PROJECT_DIR}/${KCIDB_DUMPFILE_NAME}" should include '"bar"'
            The contents of file "${CI_PROJECT_DIR}/${KCIDB_DUMPFILE_NAME}" should include '"mm"'
            The contents of file "${python_calls_file}" should include "|-m kpet --db /kpet-db patch list-files /tmp"
            The contents of file "${python_calls_file}" should include "|-m cki_lib.owners --owners-yaml-path /documentation/info/owners.yaml --output text --file-list /tmp"
            The contents of file "${python_calls_file}" should include "|-m cki_lib.owners --owners-yaml-path /documentation/info/owners.yaml --output json --file-list /tmp"
        End
    End

    Context 'if there are pathological conditions'
        export patch_urls="foo"

        It "can deal with with empty file lists"
            kpet_output=
            subsystems_output='[]'
            owners_output=()

            When run kcidb_set_modified_files_affected_subsystems
            The status should be success
            The contents of file "${CI_PROJECT_DIR}/${KCIDB_DUMPFILE_NAME}" should include '"patchset_modified_files": []'
            The contents of file "${CI_PROJECT_DIR}/${KCIDB_DUMPFILE_NAME}" should include '"affected_subsystems": []'
            The contents of file "${python_calls_file}" should include "|-m kpet --db /kpet-db patch list-files foo|"
            The contents of file "${python_calls_file}" should include "|-m cki_lib.owners --owners-yaml-path /documentation/info/owners.yaml --output text --file-list /tmp"
            The contents of file "${python_calls_file}" should include "|-m cki_lib.owners --owners-yaml-path /documentation/info/owners.yaml --output json --file-list /tmp"
        End

        It "can deal with with empty label lists"
            kpet_output=$'bar\nbaz'
            subsystems_output='[]'
            owners_output=()

            When run kcidb_set_modified_files_affected_subsystems
            The status should be success
            The contents of file "${CI_PROJECT_DIR}/${KCIDB_DUMPFILE_NAME}" should include '"bar"'
            The contents of file "${CI_PROJECT_DIR}/${KCIDB_DUMPFILE_NAME}" should include '"baz"'
            The contents of file "${CI_PROJECT_DIR}/${KCIDB_DUMPFILE_NAME}" should include '"affected_subsystems": []'
            The contents of file "${python_calls_file}" should include "|-m kpet --db /kpet-db patch list-files foo|"
            The contents of file "${python_calls_file}" should include "|-m cki_lib.owners --owners-yaml-path /documentation/info/owners.yaml --output text --file-list /tmp"
            The contents of file "${python_calls_file}" should include "|-m cki_lib.owners --owners-yaml-path /documentation/info/owners.yaml --output json --file-list /tmp"
        End

        It "filters out subsystems with the redhat label"
            kpet_output='bar'
            subsystems_output='[{"name":"maintainers","label":"redhat"}]'
            owners_output=(@redhat)

            When run kcidb_set_modified_files_affected_subsystems
            The status should be success
            The contents of file "${CI_PROJECT_DIR}/${KCIDB_DUMPFILE_NAME}" should include '"bar"'
            The contents of file "${CI_PROJECT_DIR}/${KCIDB_DUMPFILE_NAME}" should include '"affected_subsystems": []'
            The contents of file "${python_calls_file}" should include "|-m kpet --db /kpet-db patch list-files foo|"
            The contents of file "${python_calls_file}" should include "|-m cki_lib.owners --owners-yaml-path /documentation/info/owners.yaml --output text --file-list /tmp"
            The contents of file "${python_calls_file}" should include "|-m cki_lib.owners --owners-yaml-path /documentation/info/owners.yaml --output json --file-list /tmp"
        End
    End
End

Describe 'kcidb_add_selftest'
    Include tests/source_functions.sh
    Include tests/helpers.sh

    export DEBUG_KERNEL
    DEBUG_KERNEL="true"

    setup() {
        CI_PROJECT_DIR=$(mktemp -d)
        kcidb_calls_file="${CI_PROJECT_DIR}/kcidb_calls"
        touch "${kcidb_calls_file}"
    }
    BeforeEach 'setup'

    cleanup() {
        rm -rf "${CI_PROJECT_DIR}"
    }
    AfterEach 'cleanup'

    kcidb() {
        echo "kcidb ${*}" >> "${kcidb_calls_file}"
    }

    It "correctly sets the maintainers"
        When call kcidb_add_selftest "install and packaging"
        The contents of file "${kcidb_calls_file}" should include "append-dict misc/maintainers name="
        The status should be success
    End
End
