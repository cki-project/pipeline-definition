#!/bin/bash
# shellcheck disable=SC2312         # masking of return values

eval "$(shellspec - -c) exit 1"

Describe 'jq'
    Include tests/source_functions.sh
    Include tests/helpers.sh
    Set 'errexit:on' 'nounset:on' 'pipefail:on'

    Describe 'is new enough'
        It 'returns at least version 1.6 when asked'
            When call jq --version
            The output should not match pattern "jq-1.[012345]"
        End
    End
End

Describe 'ts'
    Include tests/source_functions.sh
    Include tests/helpers.sh
    Set 'errexit:on' 'nounset:on' 'pipefail:on'

    Describe 'is new enough'
        It 'has support for -s'
            Data "poohbear"
            When call ts -s
            The status should be success
            The output should match pattern "00:00:00 poohbear"
        End
    End
End

Describe 'python'
    Include tests/source_functions.sh
    Include tests/helpers.sh
    Set 'errexit:on' 'nounset:on' 'pipefail:on'

    Describe 'is new enough'
        It 'returns at least version 3.11 when asked'
            When call python3 --version
            The output should match pattern "Python 3.1[1-9].*"
        End
    End
End
