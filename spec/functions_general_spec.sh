#!/bin/bash
# shellcheck disable=SC2312         # masking of return values
# shellcheck disable=SC2317         # unreachable mocking code

eval "$(shellspec - -c) exit 1"

Describe 'base kernel configuration download'
    Include tests/source_functions.sh
    Include tests/helpers.sh
    Set 'errexit:on' 'nounset:on' 'pipefail:on'

    curl() {
        echo "${5}"
    }

    aws_s3_url() {
        echo "${2}"
    }

    Describe 'is correct kernel config file downloaded'
        ARCH_CONFIG='arch'
        It 'downloads Fedora config if variable is empty'
            When call get_base_kernel_config ""
            The output should include "${ARCH_CONFIG}.config"
        End

        It 'downloads specified config if variable is set'
            example_url="this_is_url"
            When call get_base_kernel_config "${example_url}"
            The output should include "${example_url}"
        End
    End
End

Describe 'failure extraction'
    Include tests/source_functions.sh
    Include tests/helpers.sh

    MERGELOG_PATH=merge.log
    BUILDLOG_PATH=build.log
    MERGE_FAILURE_LOG_PATH=merge_failure.log
    BUILD_FAILURE_LOG_PATH=build_failure.log
    KCIDB_DUMPFILE_NAME=kcidb_all.json

    setup() {
        CI_PROJECT_DIR=$(mktemp -d)
        cp tests/assets/stub_kcidb.json "${CI_PROJECT_DIR}/${KCIDB_DUMPFILE_NAME}"
    }
    BeforeEach 'setup'

    cleanup() {
        rm -rf "${CI_PROJECT_DIR}"
    }
    AfterEach 'cleanup'

    kcidb() {
        :
    }

    Describe 'for merge jobs'
        Parameters:dynamic
            for path in tests/assets/merge_traces/*; do
                %data "${path##*/}" "${path}"
            done
        End

        It "correctly extracts the failure from $1"
            cp "$2" "${CI_PROJECT_DIR}/${MERGELOG_PATH}"

            When call save_merge_failure
            The contents of file "${CI_PROJECT_DIR}/${MERGE_FAILURE_LOG_PATH}" \
                should equal "$(cat "$2")"
        End
    End

    Describe 'for build jobs'
        Parameters:dynamic
            for path in tests/assets/build_traces/*; do
                %data "${path##*/}" "${path}"
            done
        End

        It "correctly extracts the failure from $1"
            cp "$2" "${CI_PROJECT_DIR}/${BUILDLOG_PATH}"

            When call print_and_save_build_failure "redhat:1234"
            The contents of file "${CI_PROJECT_DIR}/${BUILD_FAILURE_LOG_PATH}" \
                should equal "$(cat "$2")"
            The lines of stdout \
                should equal "$(( 2 + $(wc -l < "$2") ))"
        End
    End
End

Describe 'targeted test extraction'
    Include tests/source_functions.sh
    Include tests/helpers.sh

    KCIDB_DUMPFILE_NAME=kcidb_all.json

    setup() {
        CI_PROJECT_DIR=$(mktemp -d)
        cp tests/assets/stub_kcidb.json "${CI_PROJECT_DIR}/${KCIDB_DUMPFILE_NAME}"
        kcidb_calls_file="${CI_PROJECT_DIR}/kcidb_calls"
        touch "${kcidb_calls_file}"
    }
    BeforeEach 'setup'

    cleanup() {
        rm -rf "${CI_PROJECT_DIR}"
    }
    AfterEach 'cleanup'

    kcidb() {
        # Only save test ID, key, value
        echo "${2} ${4} ${5}" >> "${kcidb_calls_file}"
    }

    run_logged() {
        if [[ "${9}" == "set" ]] ; then
            echo -e "secret test\nltp"
        elif [[ "${9}" == "nonexistent" ]] ; then
            echo -e "nonexistent\nltp"
        fi
    }

    Context 'if there are targeted tests'
        It "correctly sets the targeted testing flag"
            export kpet_noarch_args
            kpet_noarch_args=(set)

            When call save_targeted_testing_info
            The contents of file "${kcidb_calls_file}" should include "redhat:2 misc/targeted true"
            The contents of file "${kcidb_calls_file}" should include "redhat:3 misc/targeted true"
        End
    End

    Context 'if there are targeted tests for other architecture'
        It "does not crash"
            export kpet_noarch_args
            kpet_noarch_args=(nonexistent)

            When call save_targeted_testing_info
            The contents of file "${kcidb_calls_file}" should include "redhat:2 misc/targeted true"
            The contents of file "${kcidb_calls_file}" should not include "redhat:1 misc/targeted true"
            The contents of file "${kcidb_calls_file}" should not include "redhat:3 misc/targeted true"
        End
    End

    Context 'if there are no targeted tests'
        It "does nothing"
            When call save_targeted_testing_info
            The contents of file "${kcidb_calls_file}" should equal ""
        End
    End

    Context 'if there are no tests'
        It "does not crash"
            jq 'del(.tests)' "${CI_PROJECT_DIR}/${KCIDB_DUMPFILE_NAME}" | \
                             sponge "${CI_PROJECT_DIR}/${KCIDB_DUMPFILE_NAME}"

            When call save_targeted_testing_info
            The status should be success
            The contents of file "${kcidb_calls_file}" should equal ""
        End
    End
End

Describe 'is_production'
    Include tests/source_functions.sh
    Include tests/helpers.sh
    Set 'errexit:on' 'nounset:on' 'pipefail:on'

    It 'exits != 0 without CKI_DEPLOYMENT_ENVIRONMENT'
        unset CKI_DEPLOYMENT_ENVIRONMENT
        When call is_production
        The status should be failure
    End

    It 'exits == 0 with CKI_DEPLOYMENT_ENVIRONMENT=production'
        export CKI_DEPLOYMENT_ENVIRONMENT=production
        When call is_production
        The status should be success
    End

    It 'exits != 0 with CKI_DEPLOYMENT_ENVIRONMENT=staging'
        export CKI_DEPLOYMENT_ENVIRONMENT=staging
        When call is_production
        The status should be failure
    End
End

Describe 'is_debug_build'
    Include tests/source_functions.sh
    Include tests/helpers.sh
    Set 'errexit:on' 'nounset:on' 'pipefail:on'

    Describe 'exits != 0'
        Parameters:matrix
            'false'
            'kernel' 'kernel-rt'
        End

        It "with DEBUG_KERNEL=$1 and package_name=$2"
            export DEBUG_KERNEL=$1
            export package_name=$2
            When call is_debug_build
            The status should be failure
        End
    End

    # NOTE: Theorically "Multiple Parameters definitions are merged.",
    # but that was crashing the shellspec reporter
    It "exits == 0 with DEBUG_KERNEL='true' and package_name='kernel'"
        export DEBUG_KERNEL='true'
        export package_name='kernel'
        When call is_debug_build
        The status should be success
    End

    Describe 'exits == 0'
        Parameters:matrix
            'true' 'false'
            'kernel-debug' 'kernel-debug-rt' 'kernel-rt-debug'
        End

        It "with DEBUG_KERNEL=$1 and package_name=$2"
            export DEBUG_KERNEL=$1
            export package_name=$2
            When call is_debug_build
            The status should be success
        End
    End
End

Describe 'is_retrigger'
    Include tests/source_functions.sh
    Include tests/helpers.sh
    Set 'errexit:on' 'nounset:on' 'pipefail:on'

    It 'exits != 0 without CKI_RETRIGGER_PIPELINE'
        unset CKI_RETRIGGER_PIPELINE
        When call is_retrigger
        The status should be failure
    End

    It 'exits == 0 with CKI_RETRIGGER_PIPELINE=true'
        export CKI_RETRIGGER_PIPELINE="true"
        When call is_retrigger
        The status should be success
    End

    It 'exits != 0 with CKI_RETRIGGER_PIPELINE=false'
        export CKI_RETRIGGER_PIPELINE="false"
        When call is_retrigger
        The status should be failure
    End
End

Describe 'upload_test_bkr_xml'
    Include tests/source_functions.sh
    Include tests/helpers.sh

    export CI_PIPELINE_ID=42
    export CI_JOB_NAME=job-name
    export CI_JOB_ID=51
    export AWS_TARGET_PATH="${CI_PIPELINE_ID}/${CI_JOB_NAME}/${CI_JOB_ID}"

    setup() {
        CI_PROJECT_DIR=$(mktemp -d)
        mkdir -p "${CI_PROJECT_DIR}/artifacts"

        BEAKER_TEMPLATE_PATH="${CI_PROJECT_DIR}/artifacts/beaker.xml"
        touch "${BEAKER_TEMPLATE_PATH}"

        output_xml="${CI_PROJECT_DIR}/artifacts/testid1/beaker.xml"
    }
    BeforeEach 'setup'

    cleanup() {
        rm -rf "${CI_PROJECT_DIR}"
    }
    AfterEach 'cleanup'

    VENV_PY3(){
        echo "$@"
    }
    kcidb() {
        echo "$@"
    }

    It 'uploads filtered beaker xml'
        export VENV_PY3=VENV_PY3
        When call upload_test_bkr_xml "redhat:682533750_x86_64_upt_1"
        The first line of output should equal "-m cki.cki_tools.filter_bkr_task --beaker-xml-path ${BEAKER_TEMPLATE_PATH} --output-xml ${output_xml} -t 1"
        The second line of output should equal "test redhat:682533750_x86_64_upt_1 append-dict output_files name=beaker_xml url=:////${AWS_TARGET_PATH}/${output_xml}"
        The status should be success
    End

    It "doesn't create xml for non upt tests"
        export VENV_PY3=VENV_PY3
        When call upload_test_bkr_xml "redhat:776659575-x86_64-kernel-debug_selftest_net"
        The status should be success
    End
End

Describe 'generate_mr_refs'
    Include tests/source_functions.sh
    Include tests/helpers.sh
    Set 'errexit:on' 'nounset:on' 'pipefail:on'

    export mr_id=123
    export branch=target
    export commit_hash mr_source_branch_hash
    export CKI_DEPLOYMENT_ENVIRONMENT=production

    setup() {
        CI_PROJECT_DIR=$(mktemp -d)
        ORIGIN_DIR=$(mktemp -d)

        mkdir "${CI_PROJECT_DIR}/artifacts"

        cd "${ORIGIN_DIR}" || exit
        git_init
        git_commit '' initial
        git_commit main target-current
        git_branch draft-merge-feature-outdated
        git_branch draft-merge-feature-current
        git_branch feature-outdated
        git_branch feature-current
        git_commit feature-outdated commit-feature-outdated
        git_commit feature-current commit-feature-current
        git_merge draft-merge-feature-outdated feature-outdated draft-
        git_merge draft-merge-feature-current feature-current draft-
        git_commit main target-moved
        git_merge main feature-current final-
        git_commit main target-already-merged

        # * target-already-merged
        # *   final-merge-feature-current
        # |\
        # * | target-moved
        # | | * draft-merge-feature-current
        # | |/|
        # |/|/
        # | * commit-feature-current
        # |/
        # | * draft-merge-feature-outdated
        # |/|
        # | * commit-feature-outdated
        # |/
        # * target-current
        # * initial

        git_branch target :/target-current
        git_branch merge-requests/123/head :/commit-feature-current
        git_branch merge-requests/123/merge :/draft-merge-feature-current

        cd "${CI_PROJECT_DIR}" || true
        git_init
        git remote add origin "${ORIGIN_DIR}"
        git fetch --quiet

        if [[ "${_setup_pipeline}" == "merged-result" ]]; then
            mr_source_branch_hash=$(git rev-parse ":/commit-feature-current")
            commit_hash=$(git rev-parse ":/draft-merge-feature-current")
        else
            mr_source_branch_hash=
            commit_hash=$(git rev-parse ":/commit-feature-current")
        fi
    }
    BeforeEach 'setup'

    cleanup() {
        rm -rf "${CI_PROJECT_DIR}" "${ORIGIN_DIR}"
    }
    AfterEach 'cleanup'

    loop() {
        "$@"
    }

    # initialize a git repo in the current directory
    git_init() {
        git init --quiet .
        git symbolic-ref HEAD refs/heads/main
        git config user.name ci
        git config user.email ci@gitlab.com
        git config rerere.enabled false
    }

    # create a new branch
    # Args: <branch-name> ...
    git_branch() {
        git branch "$@" &> /dev/null
    }

    # create a new commit on a certain branch, and switch to it
    # Args: <branch-name> <commit content> [file name]
    git_commit() {
        [[ -n "$1" ]] && git checkout --quiet "$1"
        echo "$2" > "${3:-$2}"
        git add "${3:-$2}"
        git commit --quiet -m "$2"
    }

    # create a new merge commit, and switch to it
    # Args: <target-branch> <source-branch> <prefix>
    git_merge() {
        git checkout --quiet "$1"
        git merge --quiet --no-ff -m "$3merge-$2" "$2"
    }

    # temporarily switch to the git origin repo
    # Args: CLI invocation to run in the context of the origin repo
    git_origin() {
        cd "${ORIGIN_DIR}" || exit
        "$@"
        cd "${CI_PROJECT_DIR}" || true
        git fetch --quiet
    }

    Describe 'for merged-result pipelines'
        _setup_pipeline=merged-result

        It 'uses the temporary GitLab-provided merge commit for everything up-to-date'
            _gitlab_mr_commit="$(git rev-parse :/draft-merge-feature-current)"
            When call generate_mr_refs
            The status should be success
            The output should include "GitLab-provided merge commit"
            The value "$(git rev-parse cki-mr-commit-under-test)" should equal "${_gitlab_mr_commit}"
        End

        It 'uses the final GitLab-provided merge commit for retriggered pipelines if already merged'
            export CKI_RETRIGGER_PIPELINE=true
            git_origin git_branch -f target :/target-already-merged

            _gitlab_mr_commit="$(git rev-parse :/final-merge-feature-current)"
            When call generate_mr_refs
            The status should be success
            The output should include "already included"
            The value "$(git rev-parse cki-mr-commit-under-test)" should equal "${_gitlab_mr_commit}"
        End

        It 'uses the temporary GitLab-provided merge commit for an outdated target branch ref'
            git_origin git_branch -f target :/target-moved

            _gitlab_mr_commit="$(git rev-parse :/draft-merge-feature-current)"
            When call generate_mr_refs
            The status should be success
            The output should include "GitLab-provided merge commit"
            The value "$(git rev-parse cki-mr-commit-under-test)" should equal "${_gitlab_mr_commit}"
        End

        It 'fails for an outdated feature branch'
            mr_source_branch_hash=$(git rev-parse ":/commit-feature-outdated")
            commit_hash=$(git rev-parse ":/draft-merge-feature-outdated")

            When call generate_mr_refs
            The status should be failure
            The output should include "Aborting outdated pipeline"
        End

        It 'uses the outdated GitLab-provided merge commit for retriggered pipelines'
            export CKI_RETRIGGER_PIPELINE=true
            mr_source_branch_hash=$(git rev-parse ":/commit-feature-outdated")
            commit_hash=$(git rev-parse ":/draft-merge-feature-outdated")

            _gitlab_mr_commit="$(git rev-parse :/draft-merge-feature-outdated)"
            When call generate_mr_refs
            The status should be success
            The output should include "outdated feature branch"
            The output should include "GitLab-provided merge commit"
            The value "$(git rev-parse cki-mr-commit-under-test)" should equal "${_gitlab_mr_commit}"
        End
    End

    Describe 'for MR pipelines'
        It 'uses the temporary GitLab-provided merge commit for everything up-to-date'
            _gitlab_mr_commit="$(git rev-parse :/draft-merge-feature-current)"
            When call generate_mr_refs
            The status should be success
            The output should include "GitLab-provided merge commit"
            The value "$(git rev-parse cki-mr-commit-under-test)" should equal "${_gitlab_mr_commit}"
        End

        It 'uses the final GitLab-provided merge commit for retriggered pipelines if already merged'
            export CKI_RETRIGGER_PIPELINE=true
            git_origin git_branch -f target :/target-already-merged

            _gitlab_mr_commit="$(git rev-parse :/final-merge-feature-current)"
            When call generate_mr_refs
            The status should be success
            The output should include "already included"
            The value "$(git rev-parse cki-mr-commit-under-test)" should equal "${_gitlab_mr_commit}"
        End

        It 'uses a custom merge commit for an outdated target branch ref'
            git_origin git_branch -f target :/target-moved

            _target="$(git rev-parse :/target-moved)"
            _feature="$(git rev-parse :/commit-feature-current)"
            When call generate_mr_refs
            The status should be success
            The output should include "Generating merge commit"
            The value "$(git rev-parse cki-mr-commit-under-test^1)" should equal "${_target}"
            The value "$(git rev-parse cki-mr-commit-under-test^2)" should equal "${_feature}"
        End

        It 'uses a custom merge commit for an outdated GitLab merge commit'
            git_origin git_branch -f merge-requests/123/merge :/draft-merge-feature-outdated

            _target="$(git rev-parse :/target-current)"
            _feature="$(git rev-parse :/commit-feature-current)"
            When call generate_mr_refs
            The status should be success
            The output should include "Generating merge commit"
            The value "$(git rev-parse cki-mr-commit-under-test^1)" should equal "${_target}"
            The value "$(git rev-parse cki-mr-commit-under-test^2)" should equal "${_feature}"
        End

        It 'fails for a missing GitLab-provided merge commit'
            git_origin git_branch -D merge-requests/123/merge

            When call generate_mr_refs
            The status should be failure
            The output should include "Rebase the MR"
            The stderr should include "n't find remote ref merge-requests/123/merge"
        End

        It 'fails for a merge conflict'
            git_origin git_commit target merge-conflict commit-feature-current

            When call generate_mr_refs
            The status should be failure
            The output should include "Merge conflict"
            The output should include "Rebase the MR"
            The contents of file "${CI_PROJECT_DIR}/${MERGELOG_PATH}" \
                should include "Merge conflict"
        End

        It 'fails for an outdated feature branch'
            commit_hash=$(git rev-parse ":/commit-feature-outdated")

            When call generate_mr_refs
            The status should be failure
            The output should include "Aborting outdated pipeline"
        End

        It 'fails for an outdated feature branch even for retriggered pipelines'
            export CKI_RETRIGGER_PIPELINE=true
            commit_hash=$(git rev-parse ":/commit-feature-outdated")

            When call generate_mr_refs
            The status should be failure
            The output should include "Aborting outdated pipeline"
        End
    End
End

Describe 'rpm_or_compressed'
    Include tests/source_functions.sh
    Include tests/helpers.sh
    Set 'errexit:on' 'nounset:on' 'pipefail:on'

    Describe 'exits == 0 for files with extension in rpm gz bz2'
        It 'exits == 0 with a file with extension rpm'
            When call rpm_or_compressed foo.rpm
            The status should be success
        End
        It 'exits == 0 with a file with extension gz'
            When call rpm_or_compressed foo.gz
            The status should be success
        End
        It 'exits == 0 with a file with extension bz2'
            When call rpm_or_compressed foo.bz2
            The status should be success
        End
    End
    It 'exits != 0 with a file with extension log'
        When call rpm_or_compressed foo.log
        The status should be failure
    End
    It 'exits != 0 with a file with extension txt'
        When call rpm_or_compressed foo.txt
        The status should be failure
    End
    It 'exits != 0 with a file without extension'
        When call rpm_or_compressed foo
        The status should be failure
    End
End

Describe 'buildid'
    Include tests/source_functions.sh
    Include tests/helpers.sh

    It 'has a reasonable default'
        When call buildid
        The status should be success
        The output should equal ".test"
    End
    It 'works for MRs with a parent pipeline id'
        export mr_id=1 CI_PIPELINE_ID=73 mr_pipeline_id=84
        When call buildid
        The status should be success
        The output should equal ".1_84"
    End
    It 'works for MRs without a parent pipeline id'
        export mr_id=1 CI_PIPELINE_ID=73
        When call buildid
        The status should be success
        The output should equal ".1_73"
    End
    It 'marks coverage'
        export coverage=true
        When call buildid
        The status should be success
        The output should equal ".test.gcov"
    End
    It 'does not include details for coverage for MRs'
        export mr_id=1 CI_PIPELINE_ID=73 coverage=true
        When call buildid
        The status should be success
        The output should equal ".1.gcov"
    End
End

Describe 'kernel variant filtering'
    Include tests/source_functions.sh
    Include tests/helpers.sh

    It 'correctly filters out variants when both lists are nonempty'
        export RPMBUILD_VARIANTS_array=("up" "debug" "arm64_64k")
        When call remove_kernel_variants rpmbuild option debug
        The status should be success
        The array RPMBUILD_VARIANTS_array should be values "up" "arm64_64k"
    End

    It 'does not explode if the options are empty'
        export RPMBUILD_VARIANTS_array=("up" "debug" "arm64_64k")
        When call remove_kernel_variants
        The status should be success
        The array RPMBUILD_VARIANTS_array should be values "up" "debug" "arm64_64k"
    End

    It 'does not explode if the variants list is nonempty'
        export RPMBUILD_VARIANTS_array=()
        When call remove_kernel_variants option
        The status should be success
        The array RPMBUILD_VARIANTS_array should be empty
    End

    It 'keeps the full variant list if there are no matching options'
        export RPMBUILD_VARIANTS_array=("up" "debug" "arm64_64k")
        When call remove_kernel_variants option
        The status should be success
        The array RPMBUILD_VARIANTS_array should be values "up" "debug" "arm64_64k"
    End

    It 'works correctly if every option gets filtered out'
        export RPMBUILD_VARIANTS_array=("up" "debug" "arm64_64k")
        When call remove_kernel_variants rpmbuild option debug up arm64_64k
        The status should be success
        The array RPMBUILD_VARIANTS_array should be empty
    End
End

Describe 'jq -i emulation'
    Include tests/source_functions.sh
    Include tests/helpers.sh

    KCIDB_DUMPFILE_NAME=kcidb_all.json

    setup() {
        CI_PROJECT_DIR=$(mktemp -d)
        echo '{"foo":"bar"}' > "${CI_PROJECT_DIR}/${KCIDB_DUMPFILE_NAME}"
    }
    BeforeEach 'setup'

    cleanup() {
        rm -rf "${CI_PROJECT_DIR}"
    }
    AfterEach 'cleanup'

    It 'correctly passes args to jq'
        # shellcheck disable=SC2016
        When call jq_i "${CI_PROJECT_DIR}/${KCIDB_DUMPFILE_NAME}" -c --arg qux baz '.foo=$qux'
        The status should be success
        The contents of file "${CI_PROJECT_DIR}/${KCIDB_DUMPFILE_NAME}" \
            should equal '{"foo":"baz"}'
    End
End

Describe 'Saving environment vars in a JSON'
    Include tests/source_functions.sh
    Include tests/helpers.sh

    It 'writes each variable as expected'
        expected_output() { %text
            #|{
            #|  "KCIDB_BUILD_ID": "redhat:123-x86_64-kernel",
            #|  "KCIDB_CHECKOUT_ID": "redhat:123"
            #|}
        }

        When call envvars_to_json KCIDB_BUILD_ID KCIDB_CHECKOUT_ID
        The status should be success
        The output should equal "$(expected_output)"
    End
End

Describe 'array element checking'
    Include tests/source_functions.sh
    Include tests/helpers.sh

    It 'works with empty arrays'
        export array=()
        When call contained_in_array foo "${array[@]}"
        The status should be failure
    End

    It 'returns 0 if found'
        export array=(foo bar baz)
        When call contained_in_array bar "${array[@]}"
        The status should be success
    End

    It 'returns 1 if not found'
        export array=(foo bar baz)
        When call contained_in_array qux "${array[@]}"
        The status should be failure
    End
End
