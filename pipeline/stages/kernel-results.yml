---
# gitlab-yaml-shellcheck: main=../../cki_pipeline.yml

.check_results_common:
  extends: [.with_retries, .with_timeout, .with_python_image]
  stage: kernel-results
  before_script:
    - !reference [.common-before-script]
  script:
    # NOTE: Remove KCIDB_TRIAGED_DUMPFILE_NAME guards after 2025-07-01
    - |
      cki_echo_notify "Triaging tests from build: ${KCIDB_BUILD_ID}..."
      if [ -f "${CI_PROJECT_DIR}/${KCIDB_TRIAGED_DUMPFILE_NAME}" ]; then
        # Updates the KCIDB file with issue occurrences, submit them to DW if DATAWAREHOUSE_TOKEN_SUBMITTER is defined
        "${VENV_PY3}" -m cki.triager \
          --build-id "${KCIDB_BUILD_ID}" \
          --from-file "${CI_PROJECT_DIR}/${KCIDB_TRIAGED_DUMPFILE_NAME}" \
          --to-file "${CI_PROJECT_DIR}/${KCIDB_TRIAGED_DUMPFILE_NAME}" \
          ${DATAWAREHOUSE_TOKEN_SUBMITTER:+--to-dw} \
          2> "${CI_PROJECT_DIR}/${TRIAGERLOG_PATH}" \
          || echo "  Triaging failed! Check ${CI_PROJECT_DIR}/${TRIAGERLOG_PATH} for details"
        # Move issueoccurrences to the original KCIDB file if it exists
        jq 'if $triaged[0] | has("issueoccurrences")
            then . + {issueoccurrences: $triaged[0].issueoccurrences}
            else .
            end' \
          "${CI_PROJECT_DIR}/${KCIDB_DUMPFILE_NAME}" \
          --slurpfile triaged "${CI_PROJECT_DIR}/${KCIDB_TRIAGED_DUMPFILE_NAME}" | \
          sponge "${CI_PROJECT_DIR}/${KCIDB_DUMPFILE_NAME}"
      else
        echo "  Skipped because we are missing the KCIDB file"
      fi
    - |
      cki_echo_notify "Gathering a report..."
      # Fetch from DW if DATAWAREHOUSE_TOKEN_SUBMITTER is defined, otherwise use the KCIDB file if it exists
      # NOTE: use the failure-offline template for retriggered pipelines until datawarehouse#220
      if { [[ -n ${DATAWAREHOUSE_TOKEN_SUBMITTER:-} ]] && ! is_retrigger; } || ! [ -f "${CI_PROJECT_DIR}/${KCIDB_DUMPFILE_NAME}" ]; then
        "${VENV_PY3}" -m reporter --template failure-only --checkout_id "${KCIDB_CHECKOUT_ID}" --build-id "${KCIDB_BUILD_ID}"
      else
        "${VENV_PY3}" -m reporter --template failure-offline --checkout_id "${KCIDB_CHECKOUT_ID}" --build-id "${KCIDB_BUILD_ID}" \
          --from-file "${CI_PROJECT_DIR}/${KCIDB_DUMPFILE_NAME}"
      fi
  after_script:
    - !reference [.common-after-script-head]
    - !reference [.common-after-script-tail]
    # We don't want to submit kcidb file here!
    - echo "You can retry this job to take newly tagged issues into account."
  tags:
    - ${TAG_NON_PRODUCTION_PREFIX}pipeline-review-runner
  rules:
    - !reference [.skip_without_stage]
    - !reference [.skip_without_debug]
    - !reference [.skip_without_architecture]
    - when: on_success
  variables:
    ARTIFACTS: >
      artifacts/*.log
      ${SOFTWARE_VERSIONS_ARTIFACTS_PATH}

check_results i686:
  extends: [.check_results_common, .i686_variables]
  variables:
    ARTIFACT_DEPENDENCY: |-
      prepare python
      test i686
  needs:
    - {artifacts: false, job: prepare python}
    - {artifacts: false, job: test i686}

check_results x86_64:
  extends: [.check_results_common, .x86_64_variables]
  variables:
    ARTIFACT_DEPENDENCY: |-
      prepare python
      test x86_64
  needs:
    - {artifacts: false, job: prepare python}
    - {artifacts: false, job: test x86_64}

check_results x86_64 debug:
  extends: [.check_results_common, .x86_64_variables, .debug_variables]
  variables:
    ARTIFACT_DEPENDENCY: |-
      prepare python
      test x86_64 debug
  needs:
    - {artifacts: false, job: prepare python}
    - {artifacts: false, job: test x86_64 debug}

check_results ppc64le:
  extends: [.check_results_common, .ppc64le_variables]
  variables:
    ARTIFACT_DEPENDENCY: |-
      prepare python
      test ppc64le
  needs:
    - {artifacts: false, job: prepare python}
    - {artifacts: false, job: test ppc64le}

check_results ppc64le debug:
  extends: [.check_results_common, .ppc64le_variables, .debug_variables]
  variables:
    ARTIFACT_DEPENDENCY: |-
      prepare python
      test ppc64le debug
  needs:
    - {artifacts: false, job: prepare python}
    - {artifacts: false, job: test ppc64le debug}

check_results aarch64:
  extends: [.check_results_common, .aarch64_variables]
  variables:
    ARTIFACT_DEPENDENCY: |-
      prepare python
      test aarch64
  needs:
    - {artifacts: false, job: prepare python}
    - {artifacts: false, job: test aarch64}

check_results aarch64 debug:
  extends: [.check_results_common, .aarch64_variables, .debug_variables]
  variables:
    ARTIFACT_DEPENDENCY: |-
      prepare python
      test aarch64 debug
  needs:
    - {artifacts: false, job: prepare python}
    - {artifacts: false, job: test aarch64 debug}

check_results ppc64:
  extends: [.check_results_common, .ppc64_variables]
  variables:
    ARTIFACT_DEPENDENCY: |-
      prepare python
      test ppc64
  needs:
    - {artifacts: false, job: prepare python}
    - {artifacts: false, job: test ppc64}

check_results s390x:
  extends: [.check_results_common, .s390x_variables]
  variables:
    ARTIFACT_DEPENDENCY: |-
      prepare python
      test s390x
  needs:
    - {artifacts: false, job: prepare python}
    - {artifacts: false, job: test s390x}

check_results s390x debug:
  extends: [.check_results_common, .s390x_variables, .debug_variables]
  variables:
    ARTIFACT_DEPENDENCY: |-
      prepare python
      test s390x debug
  needs:
    - {artifacts: false, job: prepare python}
    - {artifacts: false, job: test s390x debug}

check_results riscv64:
  extends: [.check_results_common, .riscv64_variables]
  variables:
    ARTIFACT_DEPENDENCY: |-
      prepare python
      test riscv64
  needs:
    - {artifacts: false, job: prepare python}
    - {artifacts: false, job: test riscv64}

check_results riscv64 debug:
  extends: [.check_results_common, .riscv64_variables, .debug_variables]
  variables:
    ARTIFACT_DEPENDENCY: |-
      prepare python
      test riscv64 debug
  needs:
    - {artifacts: false, job: prepare python}
    - {artifacts: false, job: test riscv64 debug}
