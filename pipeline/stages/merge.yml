---
# gitlab-yaml-shellcheck: main=../../cki_pipeline.yml

.merge-clone-repo: |
  # Download and clone repo, update, check out ref.
  cki_echo_notify "Pulling cached ${git_url}..."
  if [ -n "${mr_id}" ] ; then
    # This happens to kernel developers way too often, so let's add a suggestion on what can go wrong...
    cki_echo_notify "If this step fails, you have likely submitted the MR against an unsupported project."
    cki_echo_notify "Is ${git_url} the correct target?"
  fi
  GIT_URL_OWNER=${git_url_cache_owner} git_cache_clone "${git_url}" "${WORKDIR}"

  cd "${WORKDIR}"
    cki_echo_notify "Updating ${git_url}..."

    if [[ $(cat /etc/os-release) = *Santiago* ]] ; then
      # RHEL6 compatibility
      git remote set-url origin "$(get_rhel6_auth_git_url "${git_url}")"
    else
      ensure_auth_git_url "${git_url}"
      git remote set-url origin "${git_url}"
    fi
    if [ -n "${mr_id}" ] ; then
      cki_echo_heading "📦 Generating MR references"
      generate_mr_refs
      if is_retrigger; then
        cki_echo_notify "  Skipping .gitlab-ci.yml freshness check for retriggered pipelines"
      elif ! git fetch origin "${commit_hash}"; then
        cki_echo_notify "  Unable to fetch ${commit_hash} to verify .gitlab-ci.yml freshness"
      elif ! git diff \
        --color \
        --exit-code \
        "cki-mr-commit-under-test..${commit_hash}" \
        -- \
        .gitlab-ci.yml; then
        cki_echo_error "  Pipeline triggered with outdated .gitlab-ci.yml configuration."
        cki_echo_error "  Rebase the MR on the target branch."
        exit 1
      fi
      # switch to (generated) merge commit
      commit_hash=$(git rev-parse "cki-mr-commit-under-test")
    else
      loop git fetch
    fi
    git checkout "${commit_hash}"
    cki_echo_heading "Successfully checked out ${commit_hash} from tree/MR."

    kcidb_checkout set git_commit_name "$(git describe "${commit_hash}")"
    kcidb_checkout set comment "$(git log --format=%B -n 1 | head -1)"
    kcidb_checkout set git_commit_hash "${commit_hash}"

    if [[ -n ${merge_branch:-} ]]; then
      if [[ -z ${merge_tree:-} ]]; then
        merge_tree=${git_url}
      fi
      cki_echo_heading "📦 Retrieving ${merge_tree}@${merge_branch} to merge with the tree."
      ensure_auth_git_url "${merge_tree}"
      loop git fetch --quiet "${merge_tree}" "${merge_branch}:cki-merge-branch"
    fi
  cd "${CI_PROJECT_DIR}"

.merge-get-tag: |
  # Get the tag of the current commit.
  SHORT_TAG=$(get_short_tag "${WORKDIR}")
  cki_echo_heading "🏷️ Generated short commit tag: ${SHORT_TAG}"

.merge-patches: |
  # If applicable, merge any trees and apply patches
  cd "${WORKDIR}"
    if [[ -n ${merge_branch:-} ]]; then
      cki_echo_heading "📦 Merging ${merge_tree}@${merge_branch} ..."
      git merge cki-merge-branch | tee -a "${CI_PROJECT_DIR}/${MERGELOG_PATH}"
    fi

    if [ -n "${patch_urls:-}" ]; then
      cki_echo_notify "Applying patches..."

      declare -i COUNTER=0
      for patch in ${patch_urls}; do
        # Download the patch and attempt to apply it.
        if [ -z "${patchwork_url:-}" ]; then
          # Use the last part of URL as filename for non-Patchwork patches.
          PATCH_FILENAME=$(basename "${patch}" | sed 's@\.patch.*@.patch@')
        else
          # All Patchwork URLs end with "mbox/" which is no fun
          PATCH_FILENAME="patch_${COUNTER}.patch"
        fi
        curl --config "${CKI_CURL_CONFIG_FILE}" --output "${PATCH_FILENAME}" "${patch}"
        cki_echo_notify "Applying ${patch}"
        git am < "${PATCH_FILENAME}" 2>&1 | tee -a "${CI_PROJECT_DIR}/${MERGELOG_PATH}"
        COUNTER+=1

        cki_echo_heading "${patch} applied successfully"
      done
    fi
  cd "${CI_PROJECT_DIR}"

.merge-create-diff: |
  # If this is a merge request, get a diff for targeted testing analysis
  if [ -n "${mr_id}" ] ; then
    cd "${WORKDIR}"
      git diff "cki-mr-commit-under-test^..cki-mr-commit-under-test" > "${CI_PROJECT_DIR}/${MR_DIFF_PATH}"
      kcidb_checkout set misc/mr/diff_url "$(artifact_url "${MR_DIFF_PATH}")"
    cd "${CI_PROJECT_DIR}"
  fi

.merge-modified-files-affected-subsystems: |
  # Determine modified files and affected subsystems from patch URLs, MR diff and owners database
  kcidb_set_modified_files_affected_subsystems

.merge-make-source-tarball: |
  # Package the source code into a tarball.
  if [[ ${make_target} == 'targz-pkg' ]]; then
    cd "${WORKDIR}"
      git archive -o "${CI_PROJECT_DIR}/${KERNEL_TARGZ_PATH}" HEAD
    cd "${CI_PROJECT_DIR}"
  fi

.merge-generate-build-plan: |
  if ! is_true "${skip_build}"; then
    # Dump build plan
    read -r -a expected_builds <<< "${architectures}"
    # Only rpm builds support noarch builds
    if  is_true "${build_noarch}" && [[ ${make_target} == 'rpm' ]]; then
      expected_builds+=("noarch")
    fi
    # TODO: https://gitlab.com/cki-project/pipeline-definition/-/issues/133
    # If we are in a debug-only pipeline, DEBUG_KERNEL is never set in the merge stage
    if is_debug_build; then
      # Reassign all `expected_builds` with debug=true
      for i in "${!expected_builds[@]}"; do
        expected_builds[i]="${expected_builds[i]} true"
      done
    else
      # Otherwise, extend the `expected_builds` array based on `debug_architectures`
      debug_architectures_array=()
      create_array_from_string debug_architectures
      for expected_build in "${expected_builds[@]}"; do
        if contained_in_array "${expected_build}" "${debug_architectures_array[@]}"; then
          expected_builds+=("${expected_build} true")
        fi
      done
    fi
    for expected_build in "${expected_builds[@]}"; do
      read -r expected_architecture expected_debug <<< "${expected_build}"

      if is_true "${expected_debug:-false}"; then
        BUILD_PACKAGE_NAME="${package_name%-debug}-debug"
      else
        BUILD_PACKAGE_NAME="${package_name}"
      fi

      KCIDB_BUILD_ID="${KCIDB_CHECKOUT_ID}-${expected_architecture}-${BUILD_PACKAGE_NAME}"
      kcidb build "${KCIDB_BUILD_ID}" create "${KCIDB_CHECKOUT_ID}"
      kcidb build "${KCIDB_BUILD_ID}" set architecture "${expected_architecture}"
      kcidb build "${KCIDB_BUILD_ID}" set-bool misc/debug "${expected_debug:-false}"
    done
  fi

.merge-make-source-rpm: |
  # Package the source code into a source RPM.
  if [[ ${make_target} == 'rpm' ]]; then
    cd "${WORKDIR}"
      cki_echo_notify "Building source RPM..."
      # Build source RPM. (Only Red Hat kernels supported.)

      if [ -n "${disttag_override}" ] ; then
        cki_echo_notify "Manual disttag override requested: ${disttag_override}"
        echo "%dist ${disttag_override}" | tee -a ~/.rpmmacros
      fi

      # Run the tests in redhat/self-test
      if is_true "${run_redhat_self_test}"; then
        cki_echo_notify "Running \"make dist-self-test\""
        # No parameters should be passed that change Makefile variables.  The tests assume
        # default values.
        make dist-self-test 2>&1 | ts -s | tee -a "${CI_PROJECT_DIR}/${BUILDLOG_PATH}" | tail -n 25
      fi

      # _BUILDCOMMIT for RHEL8 to get rid of the .g1234 part
      buildid > localversion
      make _BUILDCOMMIT= VERSION_ON_UPSTREAM=0 "${srpm_make_target}" 2>&1 | ts -s >> "${CI_PROJECT_DIR}/${BUILDLOG_PATH}"

      mv redhat/rpm/SRPMS/*.src.rpm "${ARTIFACTS_DIR}/"
      cki_echo_heading "Source RPM created successfully."

    cd "${CI_PROJECT_DIR}"

    kernel_version=$(rpm --queryformat="%{VERSION}-%{RELEASE}\n" -qp "${ARTIFACTS_DIR}/"*.src.rpm || true)
    kcidb_checkout set misc/kernel_version "${kernel_version}"

    set_checkout_package_versions
  fi

merge:
  extends: [.with_artifacts, .with_retries, .with_timeout, .with_builder_image]
  stage: merge
  variables:
    ARTIFACTS: >
      ${KERNEL_TARGZ_PATH}
      artifacts/*.src.rpm
      artifacts/*.log
      ${MR_DIFF_PATH}
      ${FILE_LIST_PATH}
      ${SOURCES_FILE_LIST_PATH}
      ${SUBSYSTEM_SOURCES_FILE_LIST_PATH}
    ARTIFACT_DEPENDENCY: |-
      prepare builder x86_64
  needs:
    - {artifacts: false, job: prepare builder x86_64}
  before_script:
    - !reference [.common-before-script]
  script:
    - kcidb_set_base_checkout_data
    - !reference [.merge-clone-repo]
    - !reference [.merge-get-tag]
    - !reference [.merge-patches]
    - !reference [.merge-create-diff]
    - !reference [.merge-modified-files-affected-subsystems]
    - kcidb_checkout set-bool valid true
    - !reference [.merge-make-source-tarball]
    - !reference [.merge-generate-build-plan]
    - !reference [.merge-make-source-rpm]
  after_script:
    - !reference [.common-after-script-head]
    - |
      # Set log_url if there are merge logs
      if [[ -f "${CI_PROJECT_DIR}/${MERGELOG_PATH}" ]] ; then
        kcidb_checkout set log_url "$(artifact_url "${MERGELOG_PATH}")"

        if ! is_true "$(kcidb_checkout get valid)" ; then
          save_merge_failure
        fi
      fi
    - |
      # If srpm generation failed, save and print failure data
      if [[ "${CI_JOB_STATUS}" == "failed" ]] && [[ ${make_target} == 'rpm' ]] && ! ls "${ARTIFACTS_DIR}/"*.src.rpm &> /dev/null ; then
        # If we only build SRPM to check configs, there are no build entries and we have to create one
        if is_true "${skip_build}"; then
          kcidb build "${KCIDB_CHECKOUT_ID}_srpm" create "${KCIDB_CHECKOUT_ID}"
        fi

        # A more compact call would be '.builds[]?.id' but RHEL6 builders don't support that
        for KCIDB_BUILD_ID in $(jq -r 'select(.builds)|.builds[].id' "${CI_PROJECT_DIR}/${KCIDB_DUMPFILE_NAME}") ; do
          kcidb build "${KCIDB_BUILD_ID}" set log_url "$(artifact_url "${BUILDLOG_PATH}")"
          kcidb build "${KCIDB_BUILD_ID}" set-bool valid false
          # Don't actually print the failure multiple times! We'll print it only once after the loop
          print_and_save_build_failure "${KCIDB_BUILD_ID}" > /dev/null
        done

        if [[ -f "${CI_PROJECT_DIR}/${BUILD_FAILURE_LOG_PATH}" ]] ; then
          cki_echo_error "SRPM generation failed, see the extracted errors below:"
          cat "${CI_PROJECT_DIR}/${BUILD_FAILURE_LOG_PATH}"
          cki_echo_error "----------------------------------------------------------------------"
        fi
      fi
    - !reference [.common-after-script-tail]
  tags:
    - ${TAG_NON_PRODUCTION_PREFIX}pipeline-merge-runner
  rules:
    - !reference [.skip_without_stage]
    - !reference [.skip_without_source]
    - when: on_success
