#!/bin/bash

set -Eeuo pipefail
shopt -s inherit_errexit

# shellcheck source-path=SCRIPTDIR/..
source tests/helpers.sh
# shellcheck source-path=SCRIPTDIR/..
source tests/source_functions.sh

ENVVARS_DUMP=$(envvars_to_json 'KCIDB_CHECKOUT_ID' 'KCIDB_BUILD_ID')

function _prepare_s3 {
    local CI_JOB_NAME="$1"
    local CI_JOB_ID="$2"
    local KCIDB_CONTENTS="$3"
    local JOB_PREFIX="${CI_PIPELINE_ID}/${CI_JOB_NAME}/${CI_JOB_ID}"
    shift 3

    aws_s3_rm BUCKET_ARTIFACTS "${JOB_PREFIX}"
    if [[ -n ${KCIDB_CONTENTS} ]]; then
        echo "${KCIDB_CONTENTS}" > /tmp/s3-upload.txt
        aws_s3_upload BUCKET_ARTIFACTS /tmp/s3-upload.txt "${JOB_PREFIX}/${KCIDB_DUMPFILE_NAME}"
    fi
    for ARTIFACT in "$@"; do
        echo "${ARTIFACT}" > /tmp/s3-upload.txt
        aws_s3_upload BUCKET_ARTIFACTS /tmp/s3-upload.txt "${JOB_PREFIX}/artifacts/${ARTIFACT}"
    done
}

function _clear_s3 {
    aws_s3_rm BUCKET_ARTIFACTS "${CI_PIPELINE_ID}" --recursive
}

function _check_s3_file {
    aws_s3_ls BUCKET_ARTIFACTS "$1" > /dev/null &
    wait $! && s='exists' || s='missing'
    OBSERVED_CONTENTS=$(aws_s3_download BUCKET_ARTIFACTS "$1" - 2> /dev/null || true)
    if [[ -n $2 ]]; then
        _check_equal "${s}" "exists" "$1 found" "$3"
        _check_equal "${OBSERVED_CONTENTS}" "$2" "$1 contents" "$3"
    else
        _check_equal "${s}" "missing" "$1 found" "$3"
    fi
}

function _check_s3_file_mime {
    OBSERVED_MIME_TYPE=$(aws_s3api_content_type BUCKET_ARTIFACTS "$1" 2> /dev/null || true)
    _check_match "${OBSERVED_MIME_TYPE}" "$2(;.*)?" "$1 mime" "$3"
}

function _check_s3 {
    local CI_JOB_NAME="$1"
    local CI_JOB_ID="$2"
    local ENVVARS_CONTENTS="$3"
    local KCIDB_CONTENTS="$4"
    local JOB_PREFIX="${CI_PIPELINE_ID}/${CI_JOB_NAME}/${CI_JOB_ID}"
    local INDEX s
    shift 4

    INDEX=$(aws_s3_download BUCKET_ARTIFACTS "${JOB_PREFIX}/index.html" - 2> /dev/null || true)

    _check_s3_file "${JOB_PREFIX}/${KCIDB_DUMPFILE_NAME}" "${KCIDB_CONTENTS}" "Is the kcidb file uploaded correctly"
    _check_s3_file "${JOB_PREFIX}/${ENVVARS_DUMPFILE_NAME}" "${ENVVARS_CONTENTS}" "Is the envvars file uploaded correctly"

    for ARTIFACT in "$@"; do
        local CONTENTS="${ARTIFACT}"
        grep -qF -- "${ARTIFACT#-}" <<< "${INDEX}" && s='found' || s='not found'
        if [[ "${ARTIFACT}" = -* ]]; then
            CONTENTS=""
            _check_equal "${s}" "not found" "${ARTIFACT} in index.html" "Is ${ARTIFACT} linked in index.html"
        else
            _check_equal "${s}" "found" "${ARTIFACT} in index.html" "Is ${ARTIFACT} linked in index.html"
        fi
        _check_s3_file "${JOB_PREFIX}/artifacts/${ARTIFACT#-}" "${CONTENTS}" "Is '${ARTIFACT#-}' uploaded correctly"
    done
}

function _check_s3_mime {
    local CI_JOB_NAME="$1"
    local CI_JOB_ID="$2"
    local MIME="$3"
    local JOB_PREFIX="${CI_PIPELINE_ID}/${CI_JOB_NAME}/${CI_JOB_ID}"
    shift 3

    for ARTIFACT in "$@"; do
        _check_s3_file_mime "${JOB_PREFIX}/artifacts/${ARTIFACT#-}" "${MIME}" "Is the mime type of '${ARTIFACT#-}' set correctly"
    done
}

function _clear_local {
    _prepare_local ''
}

function _prepare_local {
    local KCIDB_CONTENTS="$1"
    shift 1

    rm -rf "${ARTIFACTS_DIR}" "${ARTIFACTS_DIR}${ARTIFACTS_TEMP_SUFFIX}" "${CI_PROJECT_DIR:?}/${KCIDB_DUMPFILE_NAME}" "${CI_PROJECT_DIR:?}/${KCIDB_DUMPFILE_NAME}${ARTIFACTS_TEMP_SUFFIX}"
    mkdir -p "${ARTIFACTS_DIR}"
    if [[ -n ${KCIDB_CONTENTS} ]]; then
        echo "${KCIDB_CONTENTS}" > "${CI_PROJECT_DIR}/${KCIDB_DUMPFILE_NAME}"
    fi
    for ARTIFACT in "$@"; do
        local ARTIFACT_PATH="${ARTIFACTS_DIR}/${ARTIFACT}"
        mkdir -p "${ARTIFACT_PATH%/*}"
        echo "${ARTIFACT}" > "${ARTIFACT_PATH}"
    done
}

function _check_local_file {
    [[ -e $1 ]] && s='exists' || s='missing'
    OBSERVED_CONTENTS=$(cat "$1" 2> /dev/null || true)
    if [[ -n $2 ]]; then
        _check_equal "${s}" "exists" "$1 found" "$3"
        _check_equal "${OBSERVED_CONTENTS}" "$2" "$1 contents" "$3"
    else
        _check_equal "${s}" "missing" "$1 found" "$3"
    fi
}

function _check_local {
    local KCIDB_CONTENTS="$1"
    shift 1

    _check_local_file "${CI_PROJECT_DIR}/${KCIDB_DUMPFILE_NAME}" "${KCIDB_CONTENTS}" "Is the local kcidb file in the correct state"
    for ARTIFACT in "$@"; do
        local CONTENTS="${ARTIFACT}"
        if [[ "${ARTIFACT}" = -* ]]; then
            CONTENTS=""
        fi
        _check_local_file "${ARTIFACTS_DIR}/${ARTIFACT#-}" "${CONTENTS}" "Is '${ARTIFACT#-}' in the correct state"
    done

    _check_local_file "${CI_PROJECT_DIR}/${KCIDB_DUMPFILE_NAME}${ARTIFACTS_TEMP_SUFFIX}" '' "Is the temporary ${KCIDB_DUMPFILE_NAME} file removed"
    _check_local_file "${ARTIFACTS_DIR}-temp" '' "Is the temporary artifact directory removed"
}

function _check_json_key {
    [[ -e $1 ]] && s='exists' || s='missing'
    OBSERVED_CONTENTS=$(jq --arg KEY "$2" -r '.[$KEY] // "MISSING"' < "$1" 2> /dev/null || true)
    _check_equal "${s}" "exists" "$1 found" "$4"
    _check_equal "${OBSERVED_CONTENTS}" "$3" "$1 key $2" "$4"
}

function _check_artifacts_meta {
    _check_json_key "${CI_PROJECT_DIR}/${ARTIFACTS_META_PATH}" "mode" "s3" "Is the artifact mode correctly set"
    _check_json_key "${CI_PROJECT_DIR}/${ARTIFACTS_META_PATH}" "s3_url" "$1" "Is the S3 URL correctly set"
    _check_json_key "${CI_PROJECT_DIR}/${ARTIFACTS_META_PATH}" "s3_browse_url" "$2" "Is the browsable S3 URL correctly set"
    _check_json_key "${CI_PROJECT_DIR}/${ARTIFACTS_META_PATH}" "s3_index_url" "$3" "Is the index S3 URL correctly set"
}

function _check_envvars_dumpfile {
    _check_json_key "${CI_PROJECT_DIR}/${ENVVARS_DUMPFILE_NAME}" "KCIDB_BUILD_ID" "${KCIDB_BUILD_ID}" "Build ID correctly set"
    _check_json_key "${CI_PROJECT_DIR}/${ENVVARS_DUMPFILE_NAME}" "KCIDB_CHECKOUT_ID" "${KCIDB_CHECKOUT_ID}" "Checkout ID correctly set"
}

function check_upload_no_artifacts {
    echo
    cki_echo_notify "Checking artifact upload without any artifacts."
    _clear_local
    _clear_s3
    aws_s3_upload_artifacts

    _check_local ''
    _check_envvars_dumpfile
    # shellcheck disable=SC2154,SC2153  # defined via setup_minio.sh below
    _check_artifacts_meta \
        "${LOCALMINIO_ENDPOINT}/${ARTIFACTS_BUCKET}/${ARTIFACTS_PATH}${AWS_TARGET_PATH}" \
        "${LOCALMINIO_ENDPOINT}/${ARTIFACTS_BUCKET}/index.html?prefix=${ARTIFACTS_PATH}${AWS_TARGET_PATH}" \
        "${LOCALMINIO_ENDPOINT}/${ARTIFACTS_BUCKET}/${ARTIFACTS_PATH}${AWS_TARGET_PATH}/index.html"
    _check_s3 job1 1 "${ENVVARS_DUMP}" ''
}

function check_upload_only_kcidb {
    echo
    cki_echo_notify "Checking artifact upload without kcidb file."
    _prepare_local 'dummy'
    _clear_s3
    aws_s3_upload_artifacts

    _check_local 'dummy'
    _check_envvars_dumpfile
    _check_artifacts_meta \
        "${LOCALMINIO_ENDPOINT}/${ARTIFACTS_BUCKET}/${ARTIFACTS_PATH}${AWS_TARGET_PATH}" \
        "${LOCALMINIO_ENDPOINT}/${ARTIFACTS_BUCKET}/index.html?prefix=${ARTIFACTS_PATH}${AWS_TARGET_PATH}" \
        "${LOCALMINIO_ENDPOINT}/${ARTIFACTS_BUCKET}/${ARTIFACTS_PATH}${AWS_TARGET_PATH}/index.html"
    _check_s3 job1 1 "${ENVVARS_DUMP}" 'dummy'
}

function check_upload_artifacts {
    echo
    cki_echo_notify "Checking artifact upload with artifacts."
    _prepare_local 'dummy' 'test.txt'
    _clear_s3
    aws_s3_upload_artifacts

    _check_local 'dummy'
    _check_envvars_dumpfile
    _check_artifacts_meta \
        "${LOCALMINIO_ENDPOINT}/${ARTIFACTS_BUCKET}/${ARTIFACTS_PATH}${AWS_TARGET_PATH}" \
        "${LOCALMINIO_ENDPOINT}/${ARTIFACTS_BUCKET}/index.html?prefix=${ARTIFACTS_PATH}${AWS_TARGET_PATH}" \
        "${LOCALMINIO_ENDPOINT}/${ARTIFACTS_BUCKET}/${ARTIFACTS_PATH}${AWS_TARGET_PATH}/index.html"
    _check_s3 job1 1 "${ENVVARS_DUMP}" 'dummy' 'test.txt'
}

function check_upload_repeated {
    echo
    cki_echo_notify "Checking repeated artifact upload with non-size kcidb changes."
    _prepare_local 'dummy' 'test.txt'
    _clear_s3
    aws_s3_upload_artifacts

    _check_local 'dummy'
    _check_envvars_dumpfile
    _check_artifacts_meta \
        "${LOCALMINIO_ENDPOINT}/${ARTIFACTS_BUCKET}/${ARTIFACTS_PATH}${AWS_TARGET_PATH}" \
        "${LOCALMINIO_ENDPOINT}/${ARTIFACTS_BUCKET}/index.html?prefix=${ARTIFACTS_PATH}${AWS_TARGET_PATH}" \
        "${LOCALMINIO_ENDPOINT}/${ARTIFACTS_BUCKET}/${ARTIFACTS_PATH}${AWS_TARGET_PATH}/index.html"
    _check_s3 job1 1 "${ENVVARS_DUMP}" 'dummy' 'test.txt'

    _prepare_local 'dumm2' 'test.txt'
    aws_s3_upload_artifacts

    _check_local 'dumm2'
    _check_envvars_dumpfile
    _check_artifacts_meta \
        "${LOCALMINIO_ENDPOINT}/${ARTIFACTS_BUCKET}/${ARTIFACTS_PATH}${AWS_TARGET_PATH}" \
        "${LOCALMINIO_ENDPOINT}/${ARTIFACTS_BUCKET}/index.html?prefix=${ARTIFACTS_PATH}${AWS_TARGET_PATH}" \
        "${LOCALMINIO_ENDPOINT}/${ARTIFACTS_BUCKET}/${ARTIFACTS_PATH}${AWS_TARGET_PATH}/index.html"
    _check_s3 job1 1 "${ENVVARS_DUMP}" 'dumm2' 'test.txt'
}

function check_upload_artifacts_files {
    echo
    cki_echo_notify "Checking deletion of artifact files that should (not) be uploaded."
    _prepare_local 'dummy' 'archived-file.txt' 'wildcard-file.txt' 'mime.log' 'mime.config' 'skipped-file.txt'
    _clear_s3
    local ARTIFACTS="artifacts/archived-file.txt artifacts/wildcard-*.txt artifacts/mime.* artifacts/non-existant-file.txt"
    aws_s3_upload_artifacts

    _check_local 'dummy'
    _check_envvars_dumpfile
    _check_artifacts_meta \
        "${LOCALMINIO_ENDPOINT}/${ARTIFACTS_BUCKET}/${ARTIFACTS_PATH}${AWS_TARGET_PATH}" \
        "${LOCALMINIO_ENDPOINT}/${ARTIFACTS_BUCKET}/index.html?prefix=${ARTIFACTS_PATH}${AWS_TARGET_PATH}" \
        "${LOCALMINIO_ENDPOINT}/${ARTIFACTS_BUCKET}/${ARTIFACTS_PATH}${AWS_TARGET_PATH}/index.html"
    _check_s3 job1 1 "${ENVVARS_DUMP}" 'dummy' 'archived-file.txt' 'wildcard-file.txt' '-skipped-file.txt'
    _check_s3_mime job1 1 'text/plain' 'mime.log' 'mime.config'
}

function check_upload_artifacts_directories {
    echo
    cki_echo_notify "Checking deletion of artifact directories that should (not) be uploaded."
    _prepare_local 'dummy' 'archived-dir/test.txt' 'wildcard-dir/test.txt' 'skipped-dir/test.txt'
    _clear_s3
    # shellcheck disable=SC2034
    local ARTIFACTS="artifacts/archived-dir artifacts/wildcard-* artifacts/non-existent-dir"
    aws_s3_upload_artifacts

    _check_local 'dummy'
    _check_envvars_dumpfile
    _check_artifacts_meta \
        "${LOCALMINIO_ENDPOINT}/${ARTIFACTS_BUCKET}/${ARTIFACTS_PATH}${AWS_TARGET_PATH}" \
        "${LOCALMINIO_ENDPOINT}/${ARTIFACTS_BUCKET}/index.html?prefix=${ARTIFACTS_PATH}${AWS_TARGET_PATH}" \
        "${LOCALMINIO_ENDPOINT}/${ARTIFACTS_BUCKET}/${ARTIFACTS_PATH}${AWS_TARGET_PATH}/index.html"
    _check_s3 job1 1 "${ENVVARS_DUMP}" 'dummy' 'archived-dir/test.txt' 'wildcard-dir/test.txt' '-skipped-dir/test.txt'
}

function check_download_no_dependency {
    echo
    cki_echo_notify "Checking artifact download without dependency."
    _clear_local
    _clear_s3
    aws_s3_download_artifacts

    _check_local ''
}

function check_download_no_artifacts {
    echo
    cki_echo_notify "Checking artifact download without any artifacts."
    _clear_local
    _clear_s3
    local ARTIFACT_DEPENDENCY=job1
    aws_s3_download_artifacts

    _check_local ''
}

function check_download_artifacts {
    echo
    cki_echo_notify "Checking artifact download with artifacts."

    _prepare_local 'dummy'

    _clear_s3
    _prepare_s3 job1 1 'dummy' 'test.txt'
    local ARTIFACT_DEPENDENCY=job1
    aws_s3_download_artifacts

    _check_local 'dummy' 'test.txt'
}

function check_download_multiple_artifacts {
    echo
    cki_echo_notify "Checking artifact download with multiple artifacts."

    _prepare_local 'dummy2'

    _clear_s3
    _prepare_s3 job1 1 'dummy' 'test.txt'
    _prepare_s3 job2 2 'dummy2' 'test2.txt'
    local ARTIFACT_DEPENDENCY=$'job1\njob2'
    aws_s3_download_artifacts

    _check_local 'dummy2' 'test.txt' 'test2.txt'
}

function check_download_repeated {
    echo
    cki_echo_notify "Checking repeated artifact download with artifacts."

    _prepare_local 'dummy'

    _clear_s3
    _prepare_s3 job1 1 'dummy' 'test.txt'
    local ARTIFACT_DEPENDENCY=job1
    aws_s3_download_artifacts

    _check_local 'dummy' 'test.txt'

    aws_s3_download_artifacts

    _check_local 'dummy' 'test.txt'
}

function check_download_newest {
    echo
    cki_echo_notify "Checking artifact download of the newest job."

    _prepare_local 'dummy'

    _clear_s3
    _prepare_s3 job1 10 'dummy' 'test2.txt'
    _prepare_s3 job1 9 'dummy' 'test1.txt'
    # shellcheck disable=SC2034
    local ARTIFACT_DEPENDENCY=job1
    aws_s3_download_artifacts

    _check_local 'dummy' 'test2.txt'
}

function check_download_and_update_kcidb_triage_file {
    echo
    cki_echo_notify "Checking triage KCIDB file URIs point to downloaded artifacts depending on the stage."

    local CI_JOB_STAGE
    # shellcheck disable=SC2034
    local ARTIFACT_DEPENDENCY=job1
    local CONTENT_TEMPLATE='{"tests": [{"log_url": "LOG_URL"}]}'
    # shellcheck disable=SC2154 # BUCKET_ARTIFACTS is defined via setup_minio.sh below
    local REMOTE_PATH="${BUCKET_ARTIFACTS}${CI_PIPELINE_ID}/job1/7/artifacts/file%2B7.out}"
    local CONTENT_REMOTE_PATH="${CONTENT_TEMPLATE/LOG_URL/${REMOTE_PATH}}"
    local LOCAL_PATH="file://${ARTIFACTS_DIR}/file+7.out}"
    local CONTENT_LOCAL_PATH="${CONTENT_TEMPLATE/LOG_URL/${LOCAL_PATH}}"

    _clear_s3
    _prepare_s3 job1 7 "${CONTENT_REMOTE_PATH}"

    # The main KCIDB file should keep pointing to remote path
    # The triage file shouldn't be created when CI_JOB_STAGE is anything other than "kernel-results"
    _clear_local
    CI_JOB_STAGE="test"
    aws_s3_download_artifacts
    _check_local "${CONTENT_REMOTE_PATH}"
    _check_local_file "${CI_PROJECT_DIR}/${KCIDB_TRIAGED_DUMPFILE_NAME}" "" "Is the triage kcidb file missing as expected"

    # The main KCIDB file should keep pointing to remote path
    # The triage file should point to the "local path" when CI_JOB_STAGE is "kernel-results"
    _clear_local
    CI_JOB_STAGE="kernel-results"
    aws_s3_download_artifacts
    _check_local "${CONTENT_REMOTE_PATH}"
    _check_local_file "${CI_PROJECT_DIR}/${KCIDB_TRIAGED_DUMPFILE_NAME}" "${CONTENT_LOCAL_PATH}" "Is the triage kcidb file in the correct state"
}

mkdir -p "${ARTIFACTS_DIR}"

minio_setup=$(tests/setup_minio.sh)
eval "${minio_setup}"

_failed_init

check_upload_no_artifacts
check_upload_only_kcidb
check_upload_artifacts
check_upload_repeated
check_upload_artifacts_files
check_upload_artifacts_directories

check_download_no_dependency
check_download_no_artifacts
check_download_artifacts
check_download_multiple_artifacts
check_download_repeated
check_download_newest
check_download_and_update_kcidb_triage_file

rm -rf "${ARTIFACTS_DIR}"
rm -f "${CI_PROJECT_DIR}/${ENVVARS_DUMPFILE_NAME}"
rm -f "${CI_PROJECT_DIR}/${KCIDB_DUMPFILE_NAME}"
rm -f "${CI_PROJECT_DIR}/${KCIDB_TRIAGED_DUMPFILE_NAME}"
rm -f "${CI_PROJECT_DIR}/${ARTIFACTS_META_PATH}"

_failed_check
