#!/bin/bash

# disable some warnings as shellcheck is not able to see the dynamically
# imported functions via eval() and has some assumptions about variable use
# that are not valid for unit tests
# shellcheck disable=SC2034,SC2030,SC2031
# disable warnings about masked return values as this use is very common in unit tests
# shellcheck disable=SC2312
# shellcheck disable=SC2317  # disable warnings about unreachable mocking code

set -Eeuo pipefail
shopt -s inherit_errexit

# shellcheck source-path=SCRIPTDIR/..
source tests/helpers.sh
# shellcheck source-path=SCRIPTDIR/..
source tests/source_functions.sh

_failed_init

# stub aws command and record params and relevant exported env variables
function aws {
    if [[ -f /tmp/aws_params.3 ]]; then mv /tmp/aws_params.3 /tmp/aws_params.4; fi
    if [[ -f /tmp/aws_params.2 ]]; then mv /tmp/aws_params.2 /tmp/aws_params.3; fi
    if [[ -f /tmp/aws_params ]]; then mv /tmp/aws_params /tmp/aws_params.2; fi
    echo "$@" > /tmp/aws_params
    bash -c 'echo ${AWS_ACCESS_KEY_ID}' > /tmp/aws_access_key
    bash -c 'echo ${AWS_SECRET_ACCESS_KEY}' > /tmp/aws_secret_key
}

function _clear_aws {
    rm -f /tmp/aws_params* /tmp/aws_access_key /tmp/aws_secret_key
}

function _check_leaked_vars {
    local aws_var_count
    aws_var_count=$(set | grep -c ^AWS_S3_ || true)

    _check_equal "${aws_var_count}" "0" "len(env[AWS_S3_*])" "Did no environment variables leak from the AWS functions"
}

function check_bucket {
    echo
    cki_echo_notify "🟡 Checking check_bucket"
    local BUCKET="https://endpoint.url/bucket"
    eval "$(aws_vars BUCKET)"

    # shellcheck disable=SC2154
    _check_equal "${AWS_S3_BUCKET}" "bucket" "AWS_S3_BUCKET" "Is the bucket set correctly without a path"
    # shellcheck disable=SC2154
    _check_equal "${AWS_S3_PATH}" "" "AWS_S3_PATH" "Is the path set correctly without a path"
}
check_bucket

function check_bucket_slash {
    echo
    cki_echo_notify "🟡 Checking check_bucket_slash"
    local BUCKET="https://endpoint.url/bucket/"
    eval "$(aws_vars BUCKET)"

    _check_equal "${AWS_S3_BUCKET}" "bucket" "AWS_S3_BUCKET" "Is the bucket set correctly without a path but a slash"
    _check_equal "${AWS_S3_PATH}" "" "AWS_S3_PATH" "Is the path set correctly without a path but a slash"
}
check_bucket_slash

function check_bucket_slash_path {
    echo
    cki_echo_notify "🟡 Checking check_bucket_slash_path"
    local BUCKET="https://endpoint.url/bucket/path"
    eval "$(aws_vars BUCKET)"

    _check_equal "${AWS_S3_BUCKET}" "bucket" "AWS_S3_BUCKET" "Is the bucket set correctly with a path missing a slash"
    _check_equal "${AWS_S3_PATH}" "path/" "AWS_S3_PATH" "Is the path set correctly with a path missing a slash"
}
check_bucket_slash_path

function check_bucket_slash_path_slash {
    echo
    cki_echo_notify "🟡 Checking check_bucket_slash_path_slash"
    local BUCKET="https://endpoint.url/bucket/path/"
    eval "$(aws_vars BUCKET)"

    _check_equal "${AWS_S3_BUCKET}" "bucket" "AWS_S3_BUCKET" "Is the bucket set correctly with a path with a trailing slash"
    _check_equal "${AWS_S3_PATH}" "path/" "AWS_S3_PATH" "Is the path set correctly with a path with a trailing slash"
}
check_bucket_slash_path_slash

function check_bucket_slash_path_slash_subpath {
    echo
    cki_echo_notify "🟡 Checking check_bucket_slash_path_slash_subpath"
    local BUCKET="https://endpoint.url/bucket/path/subpath"
    eval "$(aws_vars BUCKET)"

    _check_equal "${AWS_S3_BUCKET}" "bucket" "AWS_S3_BUCKET" "Is the bucket set correctly with a subpath"
    _check_equal "${AWS_S3_PATH}" "path/subpath/" "AWS_S3_PATH" "Is the path set correctly with a subpath"
}
check_bucket_slash_path_slash_subpath

function check_aws {
    echo
    cki_echo_notify "🟡 Checking check_aws"
    local AWS_ACCESS_KEY_ID_exists AWS_SECRET_ACCESS_KEY_exists BUCKET="https://endpoint.url/bucket/path/"
    eval "$(aws_vars BUCKET)"

    AWS_ACCESS_KEY_ID_exists="$([[ -v AWS_ACCESS_KEY_ID ]] && echo 1 || echo 0)"
    AWS_SECRET_ACCESS_KEY_exists="$([[ -v AWS_SECRET_ACCESS_KEY ]] && echo 1 || echo 0)"

    _check_equal "${AWS_ACCESS_KEY_ID_exists}" "0" "-v AWS_ACCESS_KEY_ID" "Is the access key kept undefined if requested"
    _check_equal "${AWS_SECRET_ACCESS_KEY_exists}" "0" "-v AWS_SECRET_ACCESS_KEY" "Is the secret key kept undefined if requested"
    # shellcheck disable=SC2154
    _check_equal "${AWS_S3_ENDPOINT_PARAMS[*]}" "--endpoint-url https://endpoint.url" "AWS_S3_ENDPOINT_PARAMS" "Is the endpoint parameter correct"
    # shellcheck disable=SC2154
    _check_equal "${AWS_S3_ENDPOINT_HOST}" "endpoint.url" "AWS_S3_ENDPOINT_HOST" "Is the endpoint host correct"
    # shellcheck disable=SC2154
    _check_equal "${AWS_S3_ENDPOINT_SCHEME}" "https" "AWS_S3_ENDPOINT_SCHEME" "Is the endpoint scheme correct"
}
check_aws

function check_aws_credentials {
    echo
    cki_echo_notify "🟡 Checking check_aws_credentials"
    local BUCKET="https://endpoint.url/bucket-name/path/"
    local BUCKET_BUCKET_NAME_AWS_ACCESS_KEY_ID="access-key"
    local BUCKET_BUCKET_NAME_AWS_SECRET_ACCESS_KEY="secret-key"
    eval "$(aws_vars BUCKET)"

    _check_equal "${AWS_ACCESS_KEY_ID}" "access-key" "aws_access_key" "Is the access key correct"
    _check_equal "${AWS_SECRET_ACCESS_KEY}" "secret-key" "aws_secret_key" "Is the secret key correct"
    _check_equal "${AWS_S3_ENDPOINT_PARAMS[*]}" "--endpoint-url https://endpoint.url" "AWS_S3_ENDPOINT_PARAMS" "Is the endpoint parameter correct"
    _check_equal "${AWS_S3_ENDPOINT_HOST}" "endpoint.url" "AWS_S3_ENDPOINT_HOST" "Is the endpoint host correct"
    _check_equal "${AWS_S3_ENDPOINT_SCHEME}" "https" "AWS_S3_ENDPOINT_SCHEME" "Is the endpoint scheme correct"
}
check_aws_credentials

function check_aws_credentials_proxy {
    echo
    cki_echo_notify "🟡 Checking check_aws_credentials_proxy"
    local BUCKET="https://endpoint.url/bucket-name/path/"
    local BUCKET_BUCKET_NAME_AWS_ACCESS_KEY_ID="access-key"
    local BUCKET_BUCKET_NAME_AWS_SECRET_ACCESS_KEY="secret-key"
    local BUCKET_BUCKET_NAME_PROXY="http://proxy.url/"
    eval "$(aws_vars BUCKET)"

    _check_equal "${AWS_ACCESS_KEY_ID}" "access-key" "aws_access_key" "Is the access key correct"
    _check_equal "${AWS_SECRET_ACCESS_KEY}" "secret-key" "aws_secret_key" "Is the secret key correct"
    _check_equal "${AWS_S3_ENDPOINT_PARAMS[*]}" "--endpoint-url https://endpoint.url" "AWS_S3_ENDPOINT_PARAMS" "Is the endpoint parameter correct"
    _check_equal "${AWS_S3_ENDPOINT_HOST}" "proxy.url" "AWS_S3_ENDPOINT_HOST" "Is the proxy endpoint host correct"
    _check_equal "${AWS_S3_ENDPOINT_SCHEME}" "http" "AWS_S3_ENDPOINT_SCHEME" "Is the proxy endpoint scheme correct"
}
check_aws_credentials_proxy

function check_aws_s3_download_command {
    echo
    cki_echo_notify "🟡 Checking check_aws_s3_download_command"
    local BUCKET="https://endpoint.url/bucket/path"
    local BUCKET_BUCKET_PROXY="https://proxy.url"
    _clear_aws
    aws_s3_download BUCKET from to

    _check_leaked_vars
    _check_equal "$(cat /tmp/aws_access_key)" "" "aws_access_key" "Is the download access key correct for AWS"
    _check_equal "$(cat /tmp/aws_secret_key)" "" "aws_secret_key" "Is the download secret key correct for AWS"
    _check_equal "$(cat /tmp/aws_params)" "--endpoint-url https://endpoint.url s3 cp --no-progress s3://bucket/path/from to" "aws_params" "Is the download command correct for AWS"
}
check_aws_s3_download_command

function check_aws_s3_download_command_credentials {
    echo
    cki_echo_notify "🟡 Checking check_aws_s3_download_command_credentials"
    local BUCKET="https://endpoint.url/bucket/path"
    local BUCKET_BUCKET_PROXY="https://proxy.url"
    local BUCKET_BUCKET_AWS_ACCESS_KEY_ID="access-key"
    local BUCKET_BUCKET_AWS_SECRET_ACCESS_KEY="secret-key"
    _clear_aws
    aws_s3_download BUCKET from to

    _check_leaked_vars
    _check_equal "$(cat /tmp/aws_access_key)" "access-key" "aws_access_key" "Is the download access key correct for AWS"
    _check_equal "$(cat /tmp/aws_secret_key)" "secret-key" "aws_secret_key" "Is the download secret key correct for AWS"
    _check_equal "$(cat /tmp/aws_params)" "--endpoint-url https://endpoint.url s3 cp --no-progress s3://bucket/path/from to" "aws_params" "Is the download command correct for AWS"
}
check_aws_s3_download_command_credentials

function check_aws_s3_download_dir_command {
    echo
    cki_echo_notify "🟡 Checking check_aws_s3_download_dir_command"
    local BUCKET="https://endpoint.url/bucket/path"
    local BUCKET_BUCKET_PROXY="https://proxy.url"
    _clear_aws
    aws_s3_download_dir BUCKET from to

    _check_leaked_vars
    _check_equal "$(cat /tmp/aws_access_key)" "" "aws_access_key" "Is the sync access key correct for AWS"
    _check_equal "$(cat /tmp/aws_secret_key)" "" "aws_secret_key" "Is the sync secret key correct for AWS"
    _check_equal "$(cat /tmp/aws_params)" "--endpoint-url https://endpoint.url s3 sync --no-progress s3://bucket/path/from to --size-only --only-show-errors" "aws_params" "Is the sync command correct for AWS"
}
check_aws_s3_download_dir_command

# function() instead of function{} so that the stub functions are scoped
function check_aws_s3_download_artifacts_command() (
    echo
    cki_echo_notify "🟡 Checking check_aws_s3_download_artifacts_command"
    local BUCKET_ARTIFACTS="https://endpoint.url/bucket/path"
    local BUCKET_BUCKET_PROXY="https://proxy.url"
    local CI_PROJECT_DIR="/tmp"
    local CI_PIPELINE_ID="12345"
    local ARTIFACT_DEPENDENCY="job"
    local PREVIOUS_JOB_ID="67890"
    local ARTIFACTS_DIR="/tmp/artifacts"
    local ARTIFACTS_TEMP_SUFFIX="-temp"
    local KCIDB_DUMPFILE_NAME="kcidb_all.json"

    function aws_s3api_list_objects {
        if [[ $2 = "${CI_PIPELINE_ID}/${ARTIFACT_DEPENDENCY}/" ]]; then
            echo "path/${CI_PIPELINE_ID}/${ARTIFACT_DEPENDENCY}/${PREVIOUS_JOB_ID}/"
        else
            echo "path/${CI_PIPELINE_ID}/${ARTIFACT_DEPENDENCY}/${PREVIOUS_JOB_ID}/${KCIDB_DUMPFILE_NAME}"
            echo "path/${CI_PIPELINE_ID}/${ARTIFACT_DEPENDENCY}/${PREVIOUS_JOB_ID}/artifacts/"
        fi
    }
    _clear_aws
    echo dummy > "${CI_PROJECT_DIR}/${KCIDB_DUMPFILE_NAME}"
    echo dummy > "${CI_PROJECT_DIR}/${KCIDB_DUMPFILE_NAME}${ARTIFACTS_TEMP_SUFFIX}"
    aws_s3_download_artifacts

    _check_leaked_vars
    _check_equal "$(cat /tmp/aws_access_key)" "" "aws_access_key" "Is the sync access key correct for AWS"
    _check_equal "$(cat /tmp/aws_secret_key)" "" "aws_secret_key" "Is the sync secret key correct for AWS"
    _check_equal "$(cat /tmp/aws_params)" "--endpoint-url https://endpoint.url s3 sync --no-progress s3://bucket/path/12345/job/67890/artifacts /tmp/artifacts --size-only --only-show-errors" "aws_params" "Is the sync command correct for AWS"
)
check_aws_s3_download_artifacts_command

function check_aws_s3_upload_command {
    echo
    cki_echo_notify "🟡 Checking check_aws_s3_upload_command"
    local BUCKET="https://endpoint.url/bucket/path"
    local BUCKET_BUCKET_PROXY="https://proxy.url"
    local target_file="target"
    local args="--endpoint-url https://endpoint.url s3 cp --no-progress from s3://bucket/path/${target_file}"
    local CKI_RETRIGGER_PIPELINE=""

    _clear_aws
    aws_s3_upload BUCKET from "${target_file}"
    _check_leaked_vars
    _check_equal "$(cat /tmp/aws_access_key)" "" "aws_access_key" "Is the upload access key correct for AWS"
    _check_equal "$(cat /tmp/aws_secret_key)" "" "aws_secret_key" "Is the upload secret key correct for AWS"
    _check_equal "$(cat /tmp/aws_params)" "${args}" "aws_params" "Is the upload command correct for AWS"

    function check_tagging_on_upload {
        echo
        cki_echo_notify "🟡 Checking check_aws_s3_upload_command/check_tagging_on_upload"
        local common_args="--endpoint-url https://endpoint.url s3api put-object-tagging --bucket bucket --key path/${target_file}"
        local both_tags_set="--tagging TagSet=[{Key=retrigger,Value=true},{Key=rpm_or_compressed,Value=true}]"
        local only_retrigger_tag_set="--tagging TagSet=[{Key=retrigger,Value=true}]"
        local only_rpm_tag_set="--tagging TagSet=[{Key=rpm_or_compressed,Value=true}]"
        # Only retrigger tag set
        for fname in foobar{,.txt,.log}; do
            _clear_aws
            CKI_RETRIGGER_PIPELINE="true"
            aws_s3_upload BUCKET "${fname}" "${target_file}"
            args="${common_args} ${only_retrigger_tag_set}"
            _check_equal "$(cat /tmp/aws_params)" "${args}" "aws_params" "Is the tag command correct for AWS"
        done

        # Only rpm_or_compressed tag set
        for fname in foobar.{rpm,gz,bz2}; do
            _clear_aws
            CKI_RETRIGGER_PIPELINE="false"
            aws_s3_upload BUCKET "${fname}" "${target_file}"
            args="${common_args} ${only_rpm_tag_set}"
            _check_equal "$(cat /tmp/aws_params)" "${args}" "aws_params" "Is the tag command correct for AWS"
        done

        # Both tags set
        for fname in foobar.{rpm,gz,bz2}; do
            _clear_aws
            CKI_RETRIGGER_PIPELINE="true"
            aws_s3_upload BUCKET "${fname}" "${target_file}"
            args="${common_args} ${both_tags_set}"
            _check_equal "$(cat /tmp/aws_params)" "${args}" "aws_params" "Is the tag command correct for AWS"
        done

        # No tags set: the last api call is the upload
        for fname in foobar{,.txt,.log}; do
            _clear_aws
            CKI_RETRIGGER_PIPELINE="false"
            aws_s3_upload BUCKET "${fname}" "${target_file}"
            args="--endpoint-url https://endpoint.url s3 cp --no-progress ${fname} s3://bucket/path/${target_file}"
            _check_equal "$(cat /tmp/aws_params)" "${args}" "aws_params" "Is the last command an s3 cp for AWS"
        done
    }
    check_tagging_on_upload
}
check_aws_s3_upload_command

function check_aws_s3_upload_dir_command {
    echo
    cki_echo_notify "🟡 Checking check_aws_s3_upload_dir_command"
    local local_dir
    # Create the temporary directory local_dir containing an empty file
    # Delete the directory on exit.
    # Args:
    #    $1: name of the empty file to create
    function init_local_dir {
        local fname="$1"
        local_dir="$(mktemp -d)"
        # shellcheck disable=SC2064
        trap "rm -rf \"${local_dir}\"" EXIT
        touch "${local_dir}/${fname}"
    }
    local BUCKET="https://endpoint.url/bucket/path"
    local BUCKET_BUCKET_PROXY="https://proxy.url"
    local target_dir="to"
    local CKI_RETRIGGER_PIPELINE=""

    init_local_dir foobar
    _clear_aws
    aws_s3_upload_dir BUCKET "${local_dir}" "${target_dir}"

    _check_leaked_vars
    _check_equal "$(cat /tmp/aws_access_key)" "" "aws_access_key" "Is the sync access key correct for AWS"
    _check_equal "$(cat /tmp/aws_secret_key)" "" "aws_secret_key" "Is the sync secret key correct for AWS"
    _check_equal "$(cat /tmp/aws_params)" "--endpoint-url https://endpoint.url s3 sync --no-progress ${local_dir} s3://bucket/path/${target_dir} --size-only --only-show-errors" "aws_params" "Is the sync command correct for AWS"

    function check_tagging_on_upload {
        echo
        cki_echo_notify "🟡 Checking check_aws_s3_upload_dir_command/check_tagging_on_upload"
        local common_args="--endpoint-url https://endpoint.url s3api put-object-tagging --bucket bucket"
        local both_tags_set="--tagging TagSet=[{Key=retrigger,Value=true},{Key=rpm_or_compressed,Value=true}]"
        local only_retrigger_tag_set="--tagging TagSet=[{Key=retrigger,Value=true}]"
        local only_rpm_tag_set="--tagging TagSet=[{Key=rpm_or_compressed,Value=true}]"
        local tmp_dir fname args
        local saved_coverage="${coverage:-}"

        # Only retrigger tag set
        for fname in foobar{,.txt,.log}; do
            init_local_dir "${fname}"
            _clear_aws
            CKI_RETRIGGER_PIPELINE="true"
            aws_s3_upload_dir BUCKET "${local_dir}" "${target_dir}"
            args="${common_args} --key path/${target_dir}/${fname} ${only_retrigger_tag_set}"

            _check_equal "$(cat /tmp/aws_params)" "${args}" "aws_params" "Is the tag command correct for AWS"
        done
        # If coverage is true, .html files are not tagged
        coverage=true
        for fname in foobar.{html,log,rpm}; do
            fname=foobar.rpm
            init_local_dir "${fname}"
            _clear_aws
            CKI_RETRIGGER_PIPELINE="true"
            aws_s3_upload_dir BUCKET "${local_dir}" "${target_dir}"
            case "${fname}" in
                *.html)
                    args="--endpoint-url https://endpoint.url s3 sync --no-progress ${local_dir} s3://bucket/path/to --size-only --only-show-errors"
                    ;;
                *.log)
                    args="${common_args} --key path/${target_dir}/${fname} ${only_retrigger_tag_set}"
                    ;;
                *.rpm)
                    args="${common_args} --key path/${target_dir}/${fname} ${both_tags_set}"
                    ;;
                *)
                    ;;
            esac
            _check_equal "$(cat /tmp/aws_params)" "${args}" "aws_params" "Is the tag command correct for AWS"
        done
        coverage="${saved_coverage}"

        # Only rpm_or_compressed tag set
        for fname in foobar.{rpm,gz,bz2}; do
            init_local_dir "${fname}"
            _clear_aws
            CKI_RETRIGGER_PIPELINE="false"
            aws_s3_upload_dir BUCKET "${local_dir}" "${target_dir}"
            args="${common_args} --key path/${target_dir}/${fname} ${only_rpm_tag_set}"
            _check_equal "$(cat /tmp/aws_params)" "${args}" "aws_params" "Is the tag command correct for AWS"
        done

        # Both tags set
        for fname in foobar.{rpm,gz,bz2}; do
            init_local_dir "${fname}"
            _clear_aws
            CKI_RETRIGGER_PIPELINE="true"
            aws_s3_upload_dir BUCKET "${local_dir}" "${target_dir}"
            args="${common_args} --key path/${target_dir}/${fname} ${both_tags_set}"
            _check_equal "$(cat /tmp/aws_params)" "${args}" "aws_params" "Is the tag command correct for AWS"
        done
        # No tags set: the last api call is the sync
        for fname in foobar{,.txt,.log}; do
            init_local_dir "${fname}"
            _clear_aws
            CKI_RETRIGGER_PIPELINE="false"
            aws_s3_upload_dir BUCKET "${local_dir}" "${target_dir}"
            args="--endpoint-url https://endpoint.url s3 sync --no-progress ${local_dir} s3://bucket/path/${target_dir} --size-only --only-show-errors"
            _check_equal "$(cat /tmp/aws_params)" "${args}" "aws_params" "Is the last command an s3 sync for AWS"
        done
    }
    check_tagging_on_upload
}
check_aws_s3_upload_dir_command

function check_aws_s3_upload_artifacts_command {
    echo
    cki_echo_notify "🟡 Checking check_aws_s3_upload_artifacts_command"
    local BUCKET_ARTIFACTS="https://endpoint.url/bucket/path"
    local BUCKET_BUCKET_PROXY="https://proxy.url"
    local CI_PROJECT_DIR="/tmp"
    local CI_PIPELINE_ID="12345"
    local CI_JOB_NAME="job"
    local CI_JOB_ID="67890"
    local AWS_TARGET_PATH="${CI_PIPELINE_ID}/${CI_JOB_NAME}/${CI_JOB_ID}"
    local ARTIFACTS_DIR="/tmp/artifacts"
    local ARTIFACTS_TEMP_SUFFIX="-temp"
    local KCIDB_DUMPFILE_NAME="kcidb_all.json"
    local target_dir="${CI_PIPELINE_ID}/job/${CI_JOB_ID}"
    local common_args="--endpoint-url https://endpoint.url" args
    local CKI_RETRIGGER_PIPELINE="true"

    _clear_aws
    mkdir -p "${ARTIFACTS_DIR}"
    aws_s3_upload_artifacts

    _check_leaked_vars
    _check_equal "$(cat /tmp/aws_access_key)" "" "aws_access_key" "Is the sync access key correct for AWS"
    _check_equal "$(cat /tmp/aws_secret_key)" "" "aws_secret_key" "Is the sync secret key correct for AWS"

    args="${common_args} s3 sync --no-progress /tmp/artifacts s3://bucket/path/${target_dir}/artifacts --size-only --only-show-errors"
    _check_equal "$(cat /tmp/aws_params.4)" "${args}" "aws_params" "Is the sync command correct for AWS"

    args="${common_args} s3api list-objects --bucket bucket --prefix path/${target_dir}/"
    _check_equal "$(cat /tmp/aws_params.3)" "${args}" "aws_params" "Is the list command correct for AWS"

    args="${common_args} s3 cp --no-progress index.html s3://bucket/path/${target_dir}/index.html"
    _check_equal "$(cat /tmp/aws_params.2)" "${args}" "aws_params" "Is the index copy command correct for AWS"

    args="${common_args} s3api put-object-tagging --bucket bucket --key path/${target_dir}/index.html --tagging TagSet=[{Key=retrigger,Value=true}]"
    _check_equal "$(cat /tmp/aws_params)" "${args}" "aws_params" "Is the tag command correct for AWS"
}
check_aws_s3_upload_artifacts_command

function check_aws_s3_ls_command {
    echo
    cki_echo_notify "🟡 Checking check_aws_s3_ls_command"
    local BUCKET="https://endpoint.url/bucket/path"
    local BUCKET_BUCKET_PROXY="https://proxy.url"
    _clear_aws
    aws_s3_ls BUCKET dir

    _check_leaked_vars
    _check_equal "$(cat /tmp/aws_access_key)" "" "aws_access_key" "Is the ls access key correct for AWS"
    _check_equal "$(cat /tmp/aws_secret_key)" "" "aws_secret_key" "Is the ls secret key correct for AWS"
    _check_equal "$(cat /tmp/aws_params)" "--endpoint-url https://endpoint.url s3 ls s3://bucket/path/dir" "aws_params" "Is the ls command correct for AWS"
}
check_aws_s3_ls_command

function check_aws_s3api_list_objects_command {
    echo
    cki_echo_notify "🟡 Checking check_aws_s3api_list_objects_command"
    local BUCKET="https://endpoint.url/bucket/path"
    local BUCKET_BUCKET_PROXY="https://proxy.url"
    _clear_aws
    aws_s3api_list_objects BUCKET dir param1 param2

    _check_leaked_vars
    _check_equal "$(cat /tmp/aws_access_key)" "" "aws_access_key" "Is the ls access key correct for AWS"
    _check_equal "$(cat /tmp/aws_secret_key)" "" "aws_secret_key" "Is the ls secret key correct for AWS"
    _check_equal "$(cat /tmp/aws_params)" "--endpoint-url https://endpoint.url s3api list-objects --bucket bucket --prefix path/dir param1 param2" "aws_params" "Is the list-objects command correct for AWS"
}
check_aws_s3api_list_objects_command

function check_aws_s3_rm_command {
    echo
    cki_echo_notify "🟡 Checking check_aws_s3_rm_command"
    local BUCKET="https://endpoint.url/bucket/path"
    local BUCKET_BUCKET_PROXY="https://proxy.url"
    _clear_aws
    aws_s3_rm BUCKET dir --recursive

    _check_leaked_vars
    _check_equal "$(cat /tmp/aws_access_key)" "" "aws_access_key" "Is the ls access key correct for AWS"
    _check_equal "$(cat /tmp/aws_secret_key)" "" "aws_secret_key" "Is the ls secret key correct for AWS"
    _check_equal "$(cat /tmp/aws_params)" "--endpoint-url https://endpoint.url s3 rm s3://bucket/path/dir --recursive" "aws_params" "Is the rm command correct for AWS"
}
check_aws_s3_rm_command

function check_aws_s3_url_command {
    echo
    cki_echo_notify "🟡 Checking check_aws_s3_url_command"
    local BUCKET="https://endpoint.url/bucket/path"
    aws_url="$(aws_s3_url BUCKET dir)"

    _check_leaked_vars
    _check_equal "${aws_url}" "https://endpoint.url/bucket/path/dir" "aws_url" "Is the url correct for AWS"
}
check_aws_s3_url_command

function check_aws_s3_url_command_proxy {
    echo
    cki_echo_notify "🟡 Checking check_aws_s3_url_command_proxy"
    local BUCKET="https://endpoint.url/bucket/path"
    local BUCKET_BUCKET_PROXY="https://proxy.url"
    aws_url="$(aws_s3_url BUCKET dir)"

    _check_leaked_vars
    _check_equal "${aws_url}" "https://proxy.url/bucket/path/dir" "aws_url" "Is the url correct for AWS"
}
check_aws_s3_url_command_proxy

function check_aws_s3_browse_url_command {
    echo
    cki_echo_notify "🟡 Checking check_aws_s3_browse_url_command"
    local BUCKET="https://endpoint.url/bucket/path"
    aws_url="$(aws_s3_browse_url BUCKET dir)"

    _check_leaked_vars
    _check_equal "${aws_url}" "https://endpoint.url/bucket/index.html?prefix=path/dir" "aws_url" "Is the browse url correct for AWS"
}
check_aws_s3_browse_url_command

function check_aws_s3_browse_url_command_proxy {
    echo
    cki_echo_notify "🟡 Checking check_aws_s3_browse_url_command_proxy"
    local BUCKET="https://endpoint.url/bucket/path"
    local BUCKET_BUCKET_PROXY="https://proxy.url"
    aws_url="$(aws_s3_browse_url BUCKET dir)"

    _check_leaked_vars
    _check_equal "${aws_url}" "https://proxy.url/bucket/index.html?prefix=path/dir" "aws_url" "Is the browse url correct for AWS"
}
check_aws_s3_browse_url_command_proxy

function check_artifact_url_s3 {
    echo
    cki_echo_notify "🟡 Checking check_artifact_url_s3"
    local BUCKET_ARTIFACTS="https://endpoint.url/bucket/path"
    local CI_PIPELINE_ID="12345"
    local CI_JOB_NAME="job"
    local CI_JOB_ID="67890"
    local AWS_TARGET_PATH="${CI_PIPELINE_ID}/${CI_JOB_NAME}/${CI_JOB_ID}"
    url="$(artifact_url "artifacts/file")"

    _check_leaked_vars
    _check_equal "${url}" "https://endpoint.url/bucket/path/12345/job/67890/artifacts/file" "url" "Is the artifact url correct"
}
check_artifact_url_s3

function check_artifact_url_s3_proxy {
    echo
    cki_echo_notify "🟡 Checking check_artifact_url_s3_proxy"
    local BUCKET_ARTIFACTS="https://endpoint.url/bucket/path"
    local BUCKET_BUCKET_PROXY="https://proxy.url"
    local CI_PIPELINE_ID="12345"
    local CI_JOB_NAME="job"
    local CI_JOB_ID="67890"
    local AWS_TARGET_PATH="${CI_PIPELINE_ID}/${CI_JOB_NAME}/${CI_JOB_ID}"
    url="$(artifact_url "artifacts/file")"

    _check_leaked_vars
    _check_equal "${url}" "https://proxy.url/bucket/path/12345/job/67890/artifacts/file" "url" "Is the artifact url correct"
}
check_artifact_url_s3_proxy

function check_browsable_artifact_url_s3 {
    echo
    cki_echo_notify "🟡 Checking check_browsable_artifact_url_s3"
    local BUCKET_ARTIFACTS="https://endpoint.url/bucket/path"
    local CI_PIPELINE_ID="12345"
    local CI_JOB_NAME="job"
    local CI_JOB_ID="67890"
    local AWS_TARGET_PATH="${CI_PIPELINE_ID}/${CI_JOB_NAME}/${CI_JOB_ID}"
    url="$(browsable_artifact_url "artifacts/file")"

    _check_leaked_vars
    _check_equal "${url}" "https://endpoint.url/bucket/path/12345/job/67890/artifacts/file" "url" "Is the browsable artifact url correct"
}
check_browsable_artifact_url_s3

function check_browsable_artifact_url_s3_proxy {
    echo
    cki_echo_notify "🟡 Checking check_browsable_artifact_url_s3_proxy"
    local BUCKET_ARTIFACTS="https://endpoint.url/bucket/path"
    local BUCKET_BUCKET_PROXY="https://proxy.url"
    local CI_PIPELINE_ID="12345"
    local CI_JOB_NAME="job"
    local CI_JOB_ID="67890"
    local AWS_TARGET_PATH="${CI_PIPELINE_ID}/${CI_JOB_NAME}/${CI_JOB_ID}"
    url="$(browsable_artifact_url "artifacts/file")"

    _check_leaked_vars
    _check_equal "${url}" "https://proxy.url/bucket/path/12345/job/67890/artifacts/file" "url" "Is the browsable artifact url correct"
}
check_browsable_artifact_url_s3_proxy

function check_browsable_artifact_directory_url_s3 {
    echo
    cki_echo_notify "🟡 Checking check_browsable_artifact_directory_url_s3"
    local BUCKET_ARTIFACTS="https://endpoint.url/bucket/path"
    local CI_PIPELINE_ID="12345"
    local CI_JOB_NAME="job"
    local CI_JOB_ID="67890"
    local AWS_TARGET_PATH="${CI_PIPELINE_ID}/${CI_JOB_NAME}/${CI_JOB_ID}"
    url="$(browsable_artifact_directory_url "artifacts/dir")"

    _check_leaked_vars
    _check_equal "${url}" "https://endpoint.url/bucket/index.html?prefix=path/12345/job/67890/artifacts/dir" "url" "Is the browsable artifact directory url correct"
}
check_browsable_artifact_directory_url_s3

function check_browsable_artifact_directory_url_s3_proxy {
    echo
    cki_echo_notify "🟡 Checking check_browsable_artifact_directory_url_s3_proxy"
    local BUCKET_ARTIFACTS="https://endpoint.url/bucket/path"
    local BUCKET_BUCKET_PROXY="https://proxy.url"
    local CI_PIPELINE_ID="12345"
    local CI_JOB_NAME="job"
    local CI_JOB_ID="67890"
    local AWS_TARGET_PATH="${CI_PIPELINE_ID}/${CI_JOB_NAME}/${CI_JOB_ID}"
    url="$(browsable_artifact_directory_url "artifacts/dir")"

    _check_leaked_vars
    _check_equal "${url}" "https://proxy.url/bucket/index.html?prefix=path/12345/job/67890/artifacts/dir" "url" "Is the browsable artifact directory url correct"
}
check_browsable_artifact_directory_url_s3_proxy

_failed_check
