#!/bin/bash

set -Eeuo pipefail
shopt -s inherit_errexit

# shellcheck source-path=SCRIPTDIR/..
source tests/helpers.sh
# shellcheck source-path=SCRIPTDIR/..
source tests/source_functions.sh

# Assume passing unless specified otherwise.
PASS=1

# Set up git with a name and email.
git config --global user.name "CKI Project"
git config --global user.email "cki-project@redhat.com"

# Create a temporary git repository for use.
TMPDIR=$(mktemp -d)
GIT_DIR=${TMPDIR}/testrepo
mkdir -p "${GIT_DIR}"

# Initialize the repository and add a commit without any tags.
cd "${GIT_DIR}"
    git init
    git commit --quiet --allow-empty --message "Initial commit with no tags"
    COMMIT_SHA=$(git rev-list --max-count=1 HEAD)
cd "${OLDPWD}"

# Test without any tags present. The result should be the last 7 characters of
# the SHA.
TAG=$(get_short_tag "${GIT_DIR}")
EXPECTED_TAG="-$(echo "${COMMIT_SHA}" | cut -b-7)"
if [[ ${TAG} = "${EXPECTED_TAG}" ]]; then
    cki_echo_heading "Passed: Test with no tags"
else
    cki_echo_error "Failed: Test with no tags"
    PASS=0
fi

# Add a VERSION-RELEASE tag.
cd "${GIT_DIR}"
    git commit --quiet --allow-empty --message "Test V-R tag"
    git tag -a -m "3.10.0-1121.el7" "3.10.0-1121.el7"
cd "${OLDPWD}"

# Test a VERSION-RELEASE tag.
TAG=$(get_short_tag "${GIT_DIR}")
if [[ ${TAG} = "-1121.el7" ]]; then
    cki_echo_heading "Passed: Test with V-R tag"
else
    cki_echo_error "Failed: Test with V-R tag"
    PASS=0
fi

# Add a kernel-VERSION-RELEASE tag.
cd "${GIT_DIR}"
    git commit --quiet --allow-empty --message "Test K-V-R tag"
    git tag -a -m "kernel-5.4.5-300.fc31" "kernel-5.4.5-300.fc31"
cd "${OLDPWD}"

# Test a kernel-VERSION-RELEASE tag.
TAG=$(get_short_tag "${GIT_DIR}")
if [[ ${TAG} = "-300.fc31" ]]; then
    cki_echo_heading "Passed: Test with k-V-R tag"
else
    cki_echo_error "Failed: Test with k-V-R tag"
    PASS=0
fi

# Clean up our temporary repository.
rm -rf "${TMPDIR}"

# Fail the script if any tests failed.
if (( PASS == 0 )); then
    cki_echo_error "get_short_tag: One or more tests failed."
    exit 1
fi

cki_echo_heading "get_short_tag: All tests passed."
