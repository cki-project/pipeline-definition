#!/bin/bash
# Add any bash functions needed in tests to this file.

# Allow running the tests locally
if [[ ! -v CI_PROJECT_DIR ]]; then
    export CI_PROJECT_DIR=${PWD}
    export CI_PIPELINE_ID=42
    export CKI_CURL_CONFIG_FILE=${CKI_CURL_CONFIG_FILE:-/dev/null}
fi

# well known CI job name/id for eg. the artifact path
export CI_JOB_NAME=job1
export CI_JOB_ID=1
export CI_JOB_STAGE='test'

# unmodified from cki_pipeline.yml
export GIT_CACHE_DIR=${CI_PROJECT_DIR}/git-cache
export WORKDIR=${CI_PROJECT_DIR}/workdir
export ARTIFACTS_DIR="${CI_PROJECT_DIR}/artifacts"
export ARTIFACTS_TEMP_SUFFIX="-temp"
export ENVVARS_DUMPFILE_NAME="envvars.json"
export KCIDB_DUMPFILE_NAME="kcidb_all.json"
export KCIDB_TRIAGED_DUMPFILE_NAME="triaged_kcidb_all.json"
export ARTIFACTS_META_PATH="artifacts-meta.json"
export MERGELOG_PATH="artifacts/merge.log"
export test_runner="beaker"
export kpet_high_cost="triggered"
export domains="available"
export extra_baseline_run_set_patterns=""
export skip_beaker="false"
export patch_urls=
export FILE_LIST_PATH=artifacts/file-list.txt
export SOURCES_FILE_LIST_PATH=artifacts/sources-file-list.txt
export SUBSYSTEM_SOURCES_FILE_LIST_PATH=artifacts/subsystem-sources-file-list.txt
export force_baseline=false
export MR_DIFF_PATH=artifacts/mr.diff
export mr_id=
# modified from cki_pipeline.yml
export MAX_TRIES=5
export MAX_WAIT=1
export ARCH=x86_64
export kpet_tree_name="fedora"
export KPET_ARCH=x86_64
export KCIDB_CHECKOUT_ID="redhat:123"
export KCIDB_BUILD_ID="redhat:123-x86_64-kernel"
export KCIDB_TEST_PREFIX="upt"

function _failed_init {
    # keep track of number of failed tests
    export FAILED_FILE
    FAILED_FILE=$(mktemp)
    trap 'rm -rf "${FAILED_FILE}"' EXIT
    echo 0 > "${FAILED_FILE}"
}

function _failed_check {
    # fail the script if any tests failed.
    echo
    FAILED="$(cat "${FAILED_FILE}")"
    if (( FAILED > 0 )); then
        cki_echo_error "${FAILED} tests failed."
        exit 1
    fi
    cki_echo_heading "All tests passed."
}

function _check_equal {
    echo
    cki_echo_notify "$4?"
    echo "  Observed: $3 == $1"
    echo "  Expected: $3 == $2"
    if [[ $1 == "$2" ]]; then
        cki_echo_heading "  Result: PASS"
    else
        cki_echo_error "  Result: FAIL"
        FAILED="$(cat "${FAILED_FILE}")"
        echo "$((FAILED + 1))" > "${FAILED_FILE}"
    fi
}

function _check_match {
    echo
    cki_echo_notify "$4?"
    echo "  Observed: $3 == $1"
    echo "  Expected: $3 =~ $2"
    if [[ $1 =~ $2 ]]; then
        cki_echo_heading "  Result: PASS"
    else
        cki_echo_error "  Result: FAIL"
        FAILED="$(cat "${FAILED_FILE}")"
        echo "$((FAILED + 1))" > "${FAILED_FILE}"
    fi
}
