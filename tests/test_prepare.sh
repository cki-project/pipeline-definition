#!/bin/bash

set -Eeuo pipefail
shopt -s inherit_errexit

# shellcheck source-path=SCRIPTDIR/..
source tests/helpers.sh
# shellcheck source-path=SCRIPTDIR/..
source tests/source_functions.sh

function read_variable() {
    config=$(python3 -c 'import yaml, json; print(json.dumps(yaml.safe_load(open("cki_pipeline.yml", "rb").read())))')
    # shellcheck disable=SC2154
    if [[ ${IMAGE_NAME} == python ]]; then
        template=.with_python_image
    else
        template=.with_builder_image
    fi
    jq --arg template "${template}" --arg name "$1" -r '.[$template].variables[$name]' <<< "${config}"
}

GITLAB_COM_PACKAGES=$(read_variable GITLAB_COM_PACKAGES)
DATA_PROJECTS=$(read_variable DATA_PROJECTS)
export GITLAB_COM_PACKAGES DATA_PROJECTS
# Add some example projects to test.
export cki_lib_pip_url="git+https://gitlab.com/cki-project/cki-lib.git@main"
export kpet_db_pip_url="git+https://gitlab.com/redhat/centos-stream/tests/kernel/kpet-db.git@main"
# Additional variables normally supplied via the pipeline.
export SOFTWARE_DIR=${CI_PROJECT_DIR}/software
export SOFTWARE_ARTIFACTS_PATH=artifacts/software.tar.gz
export SOFTWARE_VERSIONS_ARTIFACTS_PATH=artifacts/software-versions.txt
export VENV_DIR=${SOFTWARE_DIR}/venv
export VENV_PY3=${VENV_DIR}/bin/python3
export ARTIFACT_DEPENDENCY=${CI_JOB_NAME}

# Verify all software is installed in ${SOFTWARE_DIR}
function verify_installed {
  cd "${SOFTWARE_DIR}"
    cki_echo_heading "Verifying directories are present."
    test -d venv

    cki_echo_heading "Verifying Python packages."
    for PROJECT in ${GITLAB_COM_PACKAGES}; do
        if [[ "${PROJECT}" =~ ^http ]]; then
            PROJECT="$(basename "${PROJECT}")"
        elif [[ "${PROJECT}" =~ (.*)\[.*\] ]]; then
            # NOTE: `pip show` doesn't support extras yet: https://github.com/pypa/pip/issues/3797
            PROJECT="${BASH_REMATCH[1]}"
        fi
        ${VENV_PY3} -m pip show "${PROJECT}"
    done
    for PROJECT in ${DATA_PROJECTS}; do
        test -d "$(basename "${PROJECT%@*}")"
    done
  cd "${OLDPWD}"

  cki_echo_heading "🥳🤩 SUCCESS!"
}

minio_setup=$(tests/setup_minio.sh)
eval "${minio_setup}"

setup_software
verify_installed
aws_s3_upload_artifacts
rm -rf "${SOFTWARE_DIR}" "${CI_PROJECT_DIR:?}/${SOFTWARE_ARTIFACTS_PATH}"

aws_s3_download_artifacts
extract_software
verify_installed

rm -rf wheel-verify-venv "${SOFTWARE_DIR}" "${ARTIFACTS_DIR}"
